//
//  AppDelegate.h
//  Thunder
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonUtils.h"
#import "XmppEndPoint.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    
    UserEntity *Me;
     XmppEndPoint * xmpp;
    
    UITabBar *gTabbar;
}
@property (nonatomic, retain) UITabBar * gTabbar;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) XmppEndPoint * xmpp;
@property (nonatomic, strong) UserEntity *Me;
- (void) notifyReceiveNewMessage;

@end

