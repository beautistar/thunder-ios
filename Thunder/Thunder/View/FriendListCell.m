//
//  FriendListCell.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "FriendListCell.h"
#import "CommonUtils.h"

@implementation FriendListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.layoutMargins = UIEdgeInsetsZero;
    self.preservesSuperviewLayoutMargins = NO;
    
    checked = NO;
    _user = APPDELEGATE.Me;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setUser : (FriendEntity *) entity {
    
    _friend = entity;
    
    _lblName.text = entity._name;
    [_imvPhoto setImageWithURL:[NSURL URLWithString:entity._photoUrl]];
    
    if (entity._requestState == 1) {
        
        [_btnAccept setHidden:NO];
        [_btnReject setHidden:NO];
        
    } else {
        
        [_btnAccept setHidden:YES];
        [_btnReject setHidden:YES];
    }
}

- (IBAction)checkAction:(id)sender {
    
    if (!checked) {
        
        _imvCheck.image = [UIImage imageNamed:@"img_checkbox_on"];
        checked = !checked;
    } else {
        
        _imvCheck.image = [UIImage imageNamed:@"img_checkbox_off"];
        checked = !checked;
    }
}

- (IBAction)inviteAction:(id)sender {
    
    if ( [self.delegate respondsToSelector:@selector(addFriend:)] ) {
        [self.delegate addFriend:_friend];
    }
}

- (IBAction)acceptAction:(id)sender {
    
    if ( [self.delegate respondsToSelector:@selector(addFriend:)] ) {
        [self.delegate addFriend:_friend];
    }
}

- (IBAction)rejectAction:(id)sender {
    
    if ( [self.delegate respondsToSelector:@selector(rejectFriend:)] ) {
        [self.delegate rejectFriend:_friend];
    }
}



@end
