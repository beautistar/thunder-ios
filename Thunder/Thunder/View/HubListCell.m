//
//  HubListCell.m
//  Thunder
//
//  Created by Developer on 11/19/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "HubListCell.h"
#import "UIImage+ImageWithColor.h"
#import <AFNetworking/AFNetworking.h>


@implementation HubListCell

@synthesize kRowCount, kInfoTextFont, kReplyTextFont, kInfoViewHeight, kReplyTopMargin, kCommentTextFont, kNameLabelHeight, kTimeLineMargin1, kTimeLineMargin2, kTimeLineMargin3, kTimeLineNameFont, kReplyOneRowHeight, kTimeLineFlagWidth, kCommentLabelHeight, kExpandButtonHeight, kTimeLineAvatarWidth, kTimeLineAvatarHeight, kTimeLineTextMaxWidth, kMinSpacingForCellLine, kNameCountryViewHeight, kTimeLineCollectionSize, kMainTimeLinePhotCellSize;

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    kExpandButtonHeight = 29;
    kNameCountryViewHeight = 21;
    kInfoViewHeight = 21;
    
    // default padding and margin
    kTimeLineMargin1 = 8;
    kTimeLineMargin2 = 10;
    kTimeLineMargin3 = 12;
    
    kInfoTextFont = [UIFont systemFontOfSize:12];
    kReplyTextFont = [UIFont systemFontOfSize:13];
    kCommentTextFont = [UIFont systemFontOfSize:14];
    kTimeLineNameFont = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
    
    kTimeLineAvatarWidth = 50;
    kTimeLineAvatarHeight = kTimeLineAvatarWidth;
    
    kTimeLineFlagWidth = 21;
    kNameLabelHeight = 21;
    kCommentLabelHeight = 20;
    
    // colleciton view
    kRowCount = 3.0;
    kMinSpacingForCellLine = 2.0;
    kTimeLineTextMaxWidth = [UIScreen mainScreen].bounds.size.width - kTimeLineMargin2 * 2 - kTimeLineAvatarWidth - kTimeLineMargin2;
    kMainTimeLinePhotCellSize = (kTimeLineTextMaxWidth - kMinSpacingForCellLine*2.0) / kRowCount;
    
    kTimeLineCollectionSize = CGSizeMake(kMainTimeLinePhotCellSize, kMainTimeLinePhotCellSize);
    
    kReplyTopMargin = 12;
    kReplyOneRowHeight = 16;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)profileImageTapped:(id)sender {
    
    
}

- (IBAction)showAllTextTapped:(id)sender {
    
}

@end

@implementation HubPhotoCollectionViewCell

@synthesize imvHubPhoto;
    

    - (void) awakeFromNib {
        [super awakeFromNib];
        self.backgroundColor = [UIColor clearColor];
    }
    
- (void) setContent : (NSString *) fileUrl {
    
//    [imvHubPhoto setImageWithURL:[NSURL URLWithString:fileUrl] placeHolderImage:[UIImage imageWithColor:[UIColor colorWithRed:95/255.0 green:153/255.0 blue:251/255.0 alpha:1.0]]];
//    
    
    
//        [imvHubPhoto setImageWithUrl(NSURL(string: fileUrl)!, placeHolderImage: UIImage.imageWithColor(UIColor(colorNamed: WBColor.colorText2), size: kTimeLineCollectionSize))
//    }
}

@end
