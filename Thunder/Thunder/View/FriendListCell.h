//
//  FriendListCell.h
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FriendEntity.h"
#import "UserEntity.h"

@protocol FriendCellDelegate <NSObject>

- (void) addFriend:(FriendEntity *) _friend;
- (void) rejectFriend:(FriendEntity *) _friend;

@end

@interface FriendListCell : UITableViewCell {
    
    BOOL checked;
    
    UserEntity *_user;
    FriendEntity * _friend;
}



@property (nonatomic, strong) id<FriendCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imvPhoto;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblState;

@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UIImageView *imvCheck;
@property (weak, nonatomic) IBOutlet UIButton *btnInvite;
@property (weak, nonatomic) IBOutlet UIButton *btnAccept;
@property (weak, nonatomic) IBOutlet UIButton *btnReject;

- (void) setUser : (FriendEntity *) entity;

@end
