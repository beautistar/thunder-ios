//
//  HubListCell.h
//  Thunder
//
//  Created by Developer on 11/19/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HubListCell : UITableViewCell

@property (nonatomic) CGFloat kExpandButtonHeight;
@property (nonatomic) CGFloat kNameCountryViewHeight;
@property (nonatomic) CGFloat kInfoViewHeight;

// default padding and margin

@property (nonatomic) CGFloat kTimeLineMargin1;
@property (nonatomic) CGFloat kTimeLineMargin2;
@property (nonatomic) CGFloat kTimeLineMargin3;

@property (nonatomic, strong) UIFont *kInfoTextFont;
@property (nonatomic, strong) UIFont *kReplyTextFont;
@property (nonatomic, strong) UIFont *kCommentTextFont;
@property (nonatomic, strong) UIFont *kTimeLineNameFont;

@property (nonatomic) CGFloat kTimeLineAvatarWidth;
@property (nonatomic) CGFloat kTimeLineAvatarHeight;
@property (nonatomic) CGFloat kTimeLineFlagWidth;
@property (nonatomic) CGFloat kNameLabelHeight;
@property (nonatomic) CGFloat kCommentLabelHeight;

// colleciton view
@property (nonatomic) CGFloat kRowCount;
@property (nonatomic) CGFloat kMinSpacingForCellLine;
@property (nonatomic) CGFloat kTimeLineTextMaxWidth;
@property (nonatomic) CGFloat kMainTimeLinePhotCellSize;
@property (nonatomic) CGSize kTimeLineCollectionSize;
@property (nonatomic) CGFloat kReplyTopMargin;
@property (nonatomic) CGFloat kReplyOneRowHeight;


@property (weak, nonatomic) IBOutlet UIImageView *imvAvatar;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UIButton *expandButton;

@property (weak, nonatomic) IBOutlet UICollectionView *listCollectionView;
@property (weak, nonatomic) IBOutlet UIImageView *imvIconHere;
@property (weak, nonatomic) IBOutlet UILabel *lblPostedTime;
@property (weak, nonatomic) IBOutlet UILabel *lblDistance;
@property (weak, nonatomic) IBOutlet UIImageView *imvIconLikeUsers;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCnt;
@property (weak, nonatomic) IBOutlet UIImageView *imvLeftMessages;
@property (weak, nonatomic) IBOutlet UILabel *lblReplyCnt;
@property (weak, nonatomic) IBOutlet UIImageView *imvBackComment;
@property (weak, nonatomic) IBOutlet UIImageView *imvLikeIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeUsers;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstReply;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondReply;
@property (weak, nonatomic) IBOutlet UILabel *lblSeparator;
@property (weak, nonatomic) IBOutlet UIView *replySuperView;
@property (weak, nonatomic) IBOutlet UIView *likeUserView;
@property (weak, nonatomic) IBOutlet UIView *replyView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *photoGridVerticalConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *expandButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replySuperViewHeightContraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replyViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoViewVerticalConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *infoViewHeightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeUserViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *likeUserViewBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *secondReplyBottomConstraint;

@end

@interface HubPhotoCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvHubPhoto;

- (void) setContent : (NSString *) fileUrl;

@end
