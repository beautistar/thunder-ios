//
//  HubInputBar.m
//  Thunder
//
//  Created by Developer on 11/21/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "HubInputBar.h"

@implementation HubInputBar

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void) addBackgroundView {
    
    CGSize size = self.frame.size;
    
    _backgroundView = [[UIView alloc] initWithFrame:CGRectMake(5, 5, size.width - 10, size.height - 10)];
    _backgroundView.backgroundColor = [UIColor whiteColor];
    _backgroundView.layer.cornerRadius = 5;
    _backgroundView.layer.masksToBounds = YES;
    
    _backgroundView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    [self addSubview:_backgroundView];
    
}

- (void) addRightButton {
    
    
    CGSize size  = self.frame.size;
    _sendButton = UIButtonTypeCustom;
    _sendButton.frame = CGRectMake(size.width - RIGHT_BUTTON_SIZE - 10, 0, RIGHT_BUTTON_SIZE, size.height);
    _sendButton.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin);
    [_sendButton setImage:_sendButtonImage forState:UIControlStateNormal];
    [_sendButton addTarget:self action:@selector(didPressSendButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_sendButton];
}

- (void) addTextView {
    
    CGSize size = _backgroundView.frame.size;
    
    _textView = [[HPGrowingTextView alloc] initWithFrame:CGRectMake(0, 4, RIGHT_BUTTON_SIZE + 12, size.height - 8) textContainer:nil];
    _textView.isScrollable = NO;
    _textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    
    _textView.minNumberOfLines = 1;
    _textView.maxNumberOfLines = 3;
    _textView.font = [UIFont systemFontOfSize:15];
    
    _textView.delegate = self;
    _textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    _textView.placeholder = _placeHolder;
    
    _textView.keyboardType = UIKeyboardTypeDefault;
    _textView.returnKeyType = UIReturnKeyDefault;
    _textView.enablesReturnKeyAutomatically = YES;
    
    _textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.backgroundView addSubview:_textView];
}

- (void) addContent {

    [self addBackgroundView];
    [self addRightButton];
    [self addTextView];
}

- (void) beginEditing {
    
    [_textView becomeFirstResponder];
}

- (void) endEditing {
    
    [_textView resignFirstResponder];
}


- (void) didPressSendButton:(UIButton *) sender {
    
    
    //tDelegate?.didPressSendButton(self)
    self.textView.text = @"";
    
}

- (void)growingTextView:(HPGrowingTextView *)growingTextView willChangeHeight:(float)height {
    
    CGFloat diff = growingTextView.frame.size.height - (CGFloat)height;
    CGRect rect = self.frame;
    rect.size.height -= diff;
    rect.origin.y += diff;
    self.frame = rect;
    
    
}

- (void) growingTextViewDidBeginEditing:(HPGrowingTextView *)growingTextView {
    
}

- (void) growingTextViewDidChange:(HPGrowingTextView *)growingTextView {
    
    NSString *text = [growingTextView.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    if ([text isEqualToString:@""]) {
        
        [_sendButton setImage:[UIImage imageNamed:_normalImage] forState:UIControlStateNormal];
    } else {
        
        [_sendButton setImage:[UIImage imageNamed:_selectedImage] forState:UIControlStateNormal];
    }
}
@end
