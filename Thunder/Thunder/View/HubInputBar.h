//
//  HubInputBar.h
//  Thunder
//
//  Created by Developer on 11/21/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HPGrowingTextView.h"

@interface HubInputBar : UIToolbar <HPGrowingTextViewDelegate> {
    

    #define RIGHT_BUTTON_SIZE 30
    
    
    
}

@property (nonatomic, strong) HPGrowingTextView *textView;

@property (nonatomic, strong) NSString *placeHolder;
@property (nonatomic, strong) UIImage *sendButtonImage;

@property (nonatomic, strong) NSString *normalImage;
@property (nonatomic, strong) NSString *selectedImage;

@property (nonatomic, strong) NSString *text;

@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, strong) UIView *backgroundView;

- (void) addContent;
- (void) addBackgroundView;
- (void) addRightButton;
- (void) addTextView;
- (void) beginEditing;
- (void) endEditing;

@end
