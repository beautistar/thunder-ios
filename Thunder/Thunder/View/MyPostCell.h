//
//  MyPostCell.h
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyPostCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imvPicture;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (weak, nonatomic) IBOutlet UIButton *btnVideoPlay;


@end
