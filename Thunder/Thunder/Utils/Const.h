//
//  Const.h
//  Thunder
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Const : NSObject

/**
 **     Userdefaults and AppDelegate Macro
 **/
#pragma mark -
#pragma mark - UserDefaults and AppDelegate

#define USERDEFAULTS [NSUserDefaults standardUserDefaults]
#define APPDELEGATE ((AppDelegate*)[[UIApplication sharedApplication] delegate])

// Array const
//pinkColor


// string const

#define SAVE_ROOT_PATH                      @"Thunder"
#define UPLOAD_PROFILE_PATH                 @"Thunder/profile_files"
#define UPLOAD_FILE_PATH                    @"Thunder/upload_filess"
#define DOWNLOAD_FILE_PATH                  @"Thunder/download_files"

#define CONN_ERROR                          @"Connect server fail.\n Please try again."

#define ALERT_FORGOTPWD_TITLE               @"Enter your email"
#define ALERT_TITLE                         @"Thunder"
#define ALERT_OK                            @"OK"
#define ALERT_CANCEL                        @"Cancel"
#define ALERT_WAITING                       @"Waiting to accept friend."


#define INPUT_USERID                        @"Please input User Id."
#define INPUT_USERNAME                      @"Please input User Name."
#define INPUT_USEREMAIL                     @"Please input Email address."
#define INPUT_VALIDEMAIL                    @"Please input valid Email address."
#define INPUT_PASSWORDCFM                   @"Please input confirm password."
#define INPUT_PASSWORD                      @"Please input Password."
#define INPUT_INVALIDPWD                    @"Password does not matches."
#define SELECT_PHOTO                        @"Please select your profile photo."


#define EXIST_USERID                        @"User Id already exist."
#define EXIST_EMAIL                         @"Email already exist."
#define UNREGISTERED_USER                   @"Unregistered User"
#define WRONG_PASSWORD                      @"Wrong password."
#define UPLOAD_FAIL                         @"Failed to upload image."

#define SUCCESS_ADDED                       @"Successfully it has been added."
#define SUCCESS_REQUEST                     @"Successfully it has been sented friend request."
#define SUCCESS_REJECT                      @"Successfully it has been rejected friend."


/**********************************************/
// message protocol string
/**********************************************/
#define KEY_SEPERATOR                        @"#"
#define KEY_ROOM_MARKER                      @"ROOM#"
#define KEY_VIDEO_MARKER                     @"VIDEO#"
#define KEY_IMAGE_MARKER                     @"IMAGE#"
#define KEY_FILE_MARKER                      @"FILE#"
#define KEY_SYSTEM_MARKER                    @"SYSTEM#"

#define KEY_TIME_SEPERTATOR                     @"#"

#define KEY_GROUPNOTI_MARKER                 @"[GROUP NOTI]\n"
#define KEY_LEAVEROOM_MARKER                 @"LEAVE**ROOM"
#define KEY_DELEGATE_MARKER                  @"DELEGATE**ROOM"
#define KEY_INVITE_MARKER                    @"INVITE**ROOM"
#define KEY_BANISH_MARKER                    @"BANISH**ROOM"
#define KEY_REQUEST_MARKER                   @"REQUEST**ROOM"
#define KEY_ADD_MARKER                       @"ADD**ROOM"

#define VIDEO_CHATTING_SENT                  @"video chat request sent."
#define VIDEO_CHATTING_ACCEPT                @"video chat accepted."
#define VIDEO_CHATTING_DECLINE               @"video chat declined."
#define VIDEO_CHATTING_CANCEL                @"video chat canceled."

#define KEY_ONLINE_SERVICEROOM               @"online"

#define TIME_AM                              @"AM"
#define TIME_PM                              @"PM"

#define GALLERY_IS_PICTURE                  @"PICTURE"      // image collection view
#define GALLERY_IS_VIDEO                     @"VIDEO"        // video collection view

//  Integer const
#define FOR_NICKNAME                        111
#define FOR_ANONYMOUS                       222
#define FROM_FRIEND                         333

// global variable
extern BOOL bAutoLogin;
extern BOOL fromSignup;
extern BOOL fromLogout;
extern BOOL bIsShownChatView;                   // if the current topviewcontroller is ChatViewController, true
extern BOOL bIsShownChatList;                   // the current topviewcontroller is ChatListViewController, true

extern BOOL bEnteredBackground;

@end
