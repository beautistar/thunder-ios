//
//  DBManager.m
//  Thunder
//
//  Created by Developer on 12/25/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "DBManager.h"

#define MAX_LOAD_CNT        20

static DBManager *sharedInstance = nil;
static sqlite3 *database = nil;

@interface DBManager ()

- (BOOL) createDB;
- (void) runQuery:(const char *) query isQueryExecutable:(BOOL) queryExecutable;
- (NSArray *) loadDataFromDB:(NSString *) query;
- (void) executeQuery:(NSString *) query;

@end

@implementation DBManager
    
+ (DBManager *) getSharedInstance {
    
    if(!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL] init];
        [sharedInstance createDB];
    }
    
    return sharedInstance;
}

- (BOOL) createDB {
    
    NSString *docsDir;
    NSArray *dirPaths;
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString:[docsDir stringByAppendingPathComponent: @"Thunder.db"]];
    
    BOOL isSuccess = YES;
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO)
    {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS ROOMTABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT, PARTICIPANTNAME TEXT, RECENTMESSAGE TEXT, RECENTTIME TEXT, RECENTCOUNTER INTEGER)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create room table %s", errMsg);
            }
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS CHATTABLE (ID INTEGER PRIMARY KEY AUTOINCREMENT, ROOMNAME TEXT, MESSAGE TEXT, SENDER INTEGER, DATE TEXT,  CURRENT INTEGER)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create chat table %s", errMsg);
            }
            
            sql_stmt = "CREATE TABLE IF NOT EXISTS BLOCKFRIEND (ID INTEGER PRIMARY KEY AUTOINCREMENT, FRIENDID INTEGER)";
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create chat table %s", errMsg);
            }
            
            sqlite3_close(database);
            NSLog(@"created database");
            return  isSuccess;
        }
        else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    
    return isSuccess;
}

-(void)runQuery:(const char *)query isQueryExecutable:(BOOL)queryExecutable {
    // Create a sqlite object.
    sqlite3 *sqlite3Database;
    
    //    // Set the database file path.
    //    NSString *databasePath = [self.documentsDirectory stringByAppendingPathComponent:self.databaseFilename];
    
    // Initialize the results array.
    if (self.arrResults != nil) {
        [self.arrResults removeAllObjects];
        self.arrResults = nil;
    }
    self.arrResults = [[NSMutableArray alloc] init];
    
    // Initialize the column names array.
    if (self.arrColumnNames != nil) {
        [self.arrColumnNames removeAllObjects];
        self.arrColumnNames = nil;
    }
    self.arrColumnNames = [[NSMutableArray alloc] init];
    
    // Open the database.
    BOOL openDatabaseResult = sqlite3_open([databasePath UTF8String], &sqlite3Database);
    if(openDatabaseResult == SQLITE_OK) {
        // Declare a sqlite3_stmt object in which will be stored the query after having been compiled into a SQLite statement.
        sqlite3_stmt *compiledStatement;
        
        // Load all data from database to memory.
        if(sqlite3_prepare_v2(sqlite3Database, query, -1, &compiledStatement, NULL) == SQLITE_OK) {
            // Check if the query is non-executable.
            if (!queryExecutable){
                // In this case data must be loaded from the database.
                
                // Declare an array to keep the data for each fetched row.
                NSMutableArray *arrDataRow;
                
                // Loop through the results and add them to the results array row by row.
                while(sqlite3_step(compiledStatement) == SQLITE_ROW) {
                    // Initialize the mutable array that will contain the data of a fetched row.
                    arrDataRow = [[NSMutableArray alloc] init];
                    
                    // Get the total number of columns.
                    int totalColumns = sqlite3_column_count(compiledStatement);
                    
                    // Go through all columns and fetch each column data.
                    for (int i=0; i<totalColumns; i++){
                        // Convert the column data to text (characters).
                        char *dbDataAsChars = (char *)sqlite3_column_text(compiledStatement, i);
                        
                        // If there are contents in the currenct column (field) then add them to the current row array.
                        if (dbDataAsChars != NULL) {
                            // Convert the characters to string.
                            [arrDataRow addObject:[NSString  stringWithUTF8String:dbDataAsChars]];
                        }
                        
                        // Keep the current column name.
                        if (self.arrColumnNames.count != totalColumns) {
                            dbDataAsChars = (char *)sqlite3_column_name(compiledStatement, i);
                            [self.arrColumnNames addObject:[NSString stringWithUTF8String:dbDataAsChars]];
                        }
                    }
                    
                    // Store each fetched data row in the results array, but first check if there is actually data.
                    if (arrDataRow.count > 0) {
                        [self.arrResults addObject:arrDataRow];
                    }
                }
            }
            else {
                // This is the case of an executable query (insert, update, ...).
                
                // Execute the query.
                int executeQueryResults = sqlite3_step(compiledStatement);
                if (executeQueryResults == SQLITE_DONE) {
                    // Keep the affected rows.
                    self.affectedRows = sqlite3_changes(sqlite3Database);
                    
                    // Keep the last inserted row ID.
                    self.lastInsertedRowID = sqlite3_last_insert_rowid(sqlite3Database);
                }
                else {
                    // If could not execute the query show the error message on the debugger.
                    NSLog(@"DB Error: %s", sqlite3_errmsg(sqlite3Database));
                }
            }
        }
        else {
            // In the database cannot be opened then show the error message on the debugger.
            NSLog(@"%s", sqlite3_errmsg(sqlite3Database));
        }
        
        // Release the compiled statement from memory.
        sqlite3_finalize(compiledStatement);
        
    }
    
    // Close the database.
    sqlite3_close(sqlite3Database);
}

- (NSArray *) loadDataFromDB:(NSString *) query{
    // Run the query and indicate that is not executable.
    // The query string is converted to a char* object.
    [self runQuery:[query UTF8String] isQueryExecutable:NO];
    
    // Returned the loaded results.
    return (NSArray *)self.arrResults;
}

//In this one, there is not a return value.
//However, the affectedRows property can be used to verify whether there were any changes or not after having executed a query.
- (void) executeQuery:(NSString *)query{
    // Run the query and indicate that is executable.
    [self runQuery:[query UTF8String] isQueryExecutable:YES];
}

//////////////////////////////////////////////////
// Save & Load Room List from(to) DB
//////////////////////////////////////////////////

// insert new record with name, dispaly_name, recent_message, recent_time, recent_counter in db
- (void) saveRoomWithName:(NSString *) name participant_name:(NSString *) participantName recent_message:(NSString *)recentMessage  recent_time:(NSString *)recentTime recent_counter:(int) recentCounter {
    
    NSArray * exit_rooms = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM ROOMTABLE WHERE NAME = '%@'", name]];
    
    if([exit_rooms count] == 0) {
        
        NSString * saveQuery = [NSString stringWithFormat:@"INSERT INTO ROOMTABLE(NAME, PARTICIPANTNAME, RECENTMESSAGE, RECENTTIME, RECENTCOUNTER) VALUES(\"%@\", \"%@\", \"%@\" , \"%@\", \"%d\")", name, participantName, recentMessage, recentTime, recentCounter];
        
        // Execute the query.
        [self executeQuery:saveQuery];
        
    } else {
        
        // update
        int _recentCounter = [[[exit_rooms objectAtIndex:0] objectAtIndex:[self.arrColumnNames indexOfObject:@"RECENTCOUNTER"]] intValue];
        _recentCounter += recentCounter;
        
        [self updateRoomWithName:name participant_name:participantName recentMessage:recentMessage recentTime:recentTime recetCounter:_recentCounter];
    }
}

// update the record's recent message, recent_time, recent_counter with name
- (void) updateRoomWithName:(NSString *) name participant_name:(NSString *)participantsName recentMessage:(NSString *)recentMessage recentTime:(NSString *) recentTime recetCounter:(int) recentCounter {
    
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE ROOMTABLE SET PARTICIPANTNAME = '%@', RECENTMESSAGE = '%@', RECENTTIME = '%@', RECENTCOUNTER = '%d' WHERE NAME = '%@'", participantsName, recentMessage, recentTime, recentCounter, name];
    
    // Execute the query.
    [self executeQuery:updateQuery];
}

- (void) updateRoomWithName:(NSString *)name participant_name:(NSString *)participantsName {
    
    NSString *updateQuery = [NSString stringWithFormat:@"UPDATE ROOMTABLE SET PARTICIPANTNAME = '%@' WHERE NAME = '%@'", participantsName, name];
    
    // Execute the query.
    [self executeQuery:updateQuery];
}

- (void) updateRoomWithName:(NSString *) name recentCounter:(int) recentCounter {
    
    NSString * updateQuery = [NSString stringWithFormat:@"UPDATE ROOMTABLE SET RECENTCOUNTER = '%d' WHERE NAME = '%@'", recentCounter, name];
    
    [self executeQuery:updateQuery];
}

- (void) removeRoomWithName:(NSString *) name  {
    
    NSString * delQuery = [NSString stringWithFormat:@"DELETE FROM ROOMTABLE WHERE NAME = '%@'", name];
    [self executeQuery:delQuery];
    
    NSString * delChatListQuery = [NSString stringWithFormat:@"DELETE FROM CHATTABLE WHERE ROOMNAME = '%@'", name];
    [self executeQuery:delChatListQuery];
}

- (NSArray *) loadRoom {
    
    NSString * query = @"SELECT * FROM ROOMTABLE";
    
    NSArray * results = [self loadDataFromDB:query];
    
    return results;
}

//////////////////////////////////////////////////
// Save & Load Chat List from(to) DB
//////////////////////////////////////////////////
/**
 **         content:message - xmpp packet string with protocol...
 **         (ID INTEGER PRIMARY KEY AUTOINCREMENT, ROOMNAME TEXT, MESSAGE TEXT, SENDER INTEGER, DATE TEXT)
 **/
- (void) saveChatWithRoomName:(NSString *) roomName content:(NSString *) message from:(int) sender date:(NSString *) datetime {
    
    
    NSArray * _exist = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM CHATTABLE WHERE MESSAGE = '%@' AND DATE = '%@'", message, datetime]];
    
    if(_exist.count == 0) {
        
        NSString * saveQuery = [NSString  stringWithFormat:@"INSERT INTO CHATTABLE(ROOMNAME, MESSAGE, SENDER, DATE) VALUES('%@', '%@', '%i', '%@')", roomName, message, sender, datetime];
        
        [self executeQuery:saveQuery];
    }
}

//////////////////////////////////////////////////
// Save & Load Chat List from(to) DB
//////////////////////////////////////////////////
/**
 **         content:message - xmpp packet string with protocol...
 **         (ID INTEGER PRIMARY KEY AUTOINCREMENT, ROOMNAME TEXT, MESSAGE TEXT, SENDER INTEGER, DATE TEXT)
 **         isCurrent = 1 : current coming message in entered room
 **/
- (void) saveChatWithRoomName:(NSString *) roomName content:(NSString *) message from:(int) sender date:(NSString *) datetime current:(int) isCurrent
{
    
    NSArray * _exist = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM CHATTABLE WHERE MESSAGE = '%@' AND DATE = '%@'", message, datetime]];
    
    if(_exist.count == 0) {
        
        NSString * saveQuery = [NSString  stringWithFormat:@"INSERT INTO CHATTABLE(ROOMNAME, MESSAGE, SENDER, DATE, CURRENT) VALUES('%@', '%@', '%i', '%@', '%i')", roomName, message, sender, datetime, isCurrent];
        
        [self executeQuery:saveQuery];
    }
}

- (NSMutableArray *) loadMessageWithRoomName:(NSString *) roomName refreshCount:(int) refreshCount {
    
    //    NSString * loadQuery = [NSString stringWithFormat:@"SELECT * FROM CHATTABLE WHERE ROOMNAME = '%@' ORDER BY ID DESC LIMIT %i", roomName, MAX_LOAD_CNT * refreshCount];
    NSString * loadQuery = [NSString stringWithFormat:@"SELECT * FROM CHATTABLE WHERE ROOMNAME = '%@' AND CURRENT = '%i' ORDER BY ID DESC LIMIT %i", roomName, 0,  MAX_LOAD_CNT * refreshCount];
    NSArray * results = [self loadDataFromDB:loadQuery];
    
    NSMutableArray * recentList = [NSMutableArray array];
    
    if(results.count > MAX_LOAD_CNT*(refreshCount-1)) {
        
        for(int i = (int)(results.count-1); i >= MAX_LOAD_CNT*(refreshCount-1); i --) {
            NSString * msg = [[results objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"MESSAGE"]];
            NSString * sender = [[results objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"SENDER"]];
            
//            XmppPacket * _loadPacket = [[XmppPacket alloc] init];
//            _loadPacket = [_loadPacket fromStringWithProtocol:msg from:sender];
////
//            [recentList addObject:_loadPacket];
        }
    }
    
    return recentList;
}

/**
 **     when user go out room, update all coming messages  from current = 1 to current = 0
 **     when app is terminated ( in case of non-normal )
 **/
- (void) updateChatNoCurrentWithRoomName:(NSString *) roomName {
    
    NSString * updateQuery = [NSString stringWithFormat:@"UPDATE CHATTABLE SET CURRENT = '%i' WHERE CURRENT = '%i' AND ROOMNAME = '%@'", 0, 1, roomName];
    
    [self executeQuery:updateQuery];
}

- (void) updateChatNoCurrent {
    
    NSString * updateQuery = [NSString stringWithFormat:@"UPDATE CHATTABLE SET CURRENT = '%i' WHERE CURRENT = '%i'", 0, 1];
    
    [self executeQuery:updateQuery];
}

- (void) clearDB {
    
    NSString * delQuery = @"DELETE FROM ROOMTABLE";
    [self executeQuery:delQuery];
    
    delQuery = @"DELETE FROM CHATTABLE";
    [self executeQuery:delQuery];
   
}

- (void) addBlockFriend:(int) friend_id {
    
    NSArray * exit_friend = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM BLOCKFRIEND WHERE FRIENDID = '%i'", friend_id]];
    
    if([exit_friend count] != 0) {
        
        NSString * saveQuery = [NSString  stringWithFormat:@"INSERT INTO BLOCKFRIEND (FRIENDID) VALUES('%i')", friend_id];
        
        [self executeQuery:saveQuery];
    }
}

- (void) deleteBlockFriend:(int) friend_id {
    
    NSArray * exit_friend = [self loadDataFromDB:[NSString stringWithFormat:@"SELECT * FROM BLOCKFRIEND WHERE FRIENDID = '%i'", friend_id]];
    
    if([exit_friend count] == 0) {
        
        NSString * deleteQuery = [NSString  stringWithFormat:@"DELETE FROM BLOCKFRIEND WHERE FRIENDID = '%i'", friend_id];
        
        [self executeQuery:deleteQuery];
    }
}

@end
