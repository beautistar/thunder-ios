//
//  CommonUtils.m
//  Thunder
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "CommonUtils.h"
#import <AudioToolbox/AudioToolbox.h>

@implementation CommonUtils

-(id) init {
    
    self = [super init];
    if(self) {
        // custom init code
    }
    
    return self;
}

+ (BOOL) isValidEmail: (NSString *) email {
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

// resize image with specified size
+ (UIImage *) imageResize: (UIImage *) srcImage {
    
    return [srcImage resizedImageByMagick:@"128x128#"];
}

+ (UIImage *) imageResizeforChat: (UIImage *) scrImage {
    
    return [scrImage resizedImageByMagick:@"256x410#"];
}


// save image to file (Documents/Campus/profile.png
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile {
    
    NSString * savedPhotoURL = nil;
    
    NSString * outputFileName = @"";
    UIImage * outputImage;
    
    NSLog(@"scr width = %f, scr height = %f", srcImage.size.width, srcImage.size.height);
    
    // set output file name and resize source image with output image size
    if(isProfile) {
        
        outputFileName = @"profile.jpg";
        outputImage = [CommonUtils imageResize:srcImage];
        
    }
    else {
        
        outputFileName = @"chatfile.jpg";
        outputImage = [CommonUtils imageResizeforChat:srcImage];
    }
    
    NSLog(@"out width = %f, out height = %f", outputImage.size.width, outputImage.size.height);
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // current document directory
    [fileManager changeCurrentDirectoryPath:documentsDirectory];
    [fileManager createDirectoryAtPath:SAVE_ROOT_PATH withIntermediateDirectories:YES attributes:nil error:NULL];
    
    // Documents/Campus
    documentsDirectory = [documentsDirectory stringByAppendingPathComponent:SAVE_ROOT_PATH];
    NSString * filePath = [documentsDirectory stringByAppendingPathComponent:outputFileName];
    
    NSLog(@"save filePath = %@", filePath);
    
    // if the file exists already, delete and write, else if create filePath
    if([fileManager fileExistsAtPath:filePath]) {
        [fileManager removeItemAtPath:filePath error:nil];
    } else {
        [fileManager createFileAtPath:filePath contents:nil attributes:nil];
    }
    
    // Write a UIImage to PNG file
    //    [UIImagePNGRepresentation(outputImage) writeToFile:filePath atomically:YES];
    //    if(isProfile) {
    [UIImageJPEGRepresentation(outputImage, 1.0) writeToFile:filePath atomically:YES];
    //    } else {
    //        [UIImageJPEGRepresentation(outputImage, 0.8) writeToFile:filePath atomically:YES];
    //    }
    
    NSError * error;
    [fileManager contentsOfDirectoryAtPath:documentsDirectory error:&error];
    
    return savedPhotoURL = filePath;
}

+ (void) createUploadFolder {
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // Documents/D-Chat
    NSString * uploadPath = [documentsDirectory stringByAppendingPathComponent:UPLOAD_FILE_PATH];
    
    if(![fileManager fileExistsAtPath:uploadPath]) {
        [fileManager createDirectoryAtPath:uploadPath withIntermediateDirectories:YES attributes:nil error:NULL];
        //        NSLog(@"create upload folder");
    } else {
        //        NSLog(@"already exist upload folder");
    }
}

+ (void) createDownloadFolder {
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // Documents/D-Chat
    NSString * uploadPath = [documentsDirectory stringByAppendingPathComponent:DOWNLOAD_FILE_PATH];
    
    if(![fileManager fileExistsAtPath:uploadPath]) {
        [fileManager createDirectoryAtPath:uploadPath withIntermediateDirectories:YES attributes:nil error:NULL];
        
        //        NSLog(@"create download folder");
    } else {
        //        NSLog(@"already exist download folder");
    }
}

+ (BOOL) isExistDownloadFile:(NSString *) fileName {
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // Documents/D-Chat
    NSString * downloadPath = [documentsDirectory stringByAppendingPathComponent:DOWNLOAD_FILE_PATH];
    downloadPath = [downloadPath stringByAppendingPathComponent:fileName];
    
    if([fileManager fileExistsAtPath:downloadPath]) {
        return YES;
    }
    return NO;
}

+ (BOOL) isExistUploadFile:(NSString *) fileName {
    
    NSFileManager * fileManager = [NSFileManager defaultManager];
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    
    // Documents/D-Chat
    NSString * downloadPath = [documentsDirectory stringByAppendingPathComponent:UPLOAD_FILE_PATH];
    downloadPath = [downloadPath stringByAppendingPathComponent:fileName];
    
    if([fileManager fileExistsAtPath:downloadPath]) {
        return YES;
    }
    return NO;
}

+ (void) saveUserInfo {
    
    [CommonUtils setuserIdx:APPDELEGATE.Me._idx];
    [CommonUtils setUserEmail:APPDELEGATE.Me._email];
    [CommonUtils setUserPassword:APPDELEGATE.Me._password];
    [CommonUtils setUserID:APPDELEGATE.Me._userName];
    
}
+ (void) loadUserInfo {
    
    if (APPDELEGATE.Me) {
        
        APPDELEGATE.Me._idx = [CommonUtils getUserIdx];
        APPDELEGATE.Me._userName = [CommonUtils getUserName];
        APPDELEGATE.Me._email = [CommonUtils getUserEmail];
        APPDELEGATE.Me._password = [CommonUtils getUserPassword];
    }
}

+ (void) setUserID : (NSString *) ID {
    
    [UserDefault setStringValue:PREFKEY_USERID value:ID];
}

+ (NSString *) getUserID {

    return [UserDefault getStringValue:PREFKEY_USERID];
}

+ (void) setUserName : (NSString *) username {
    
     [UserDefault setStringValue:PREFKEY_USERNAME value:username];
    
}

+ (NSString *) getUserName {
    
    return [UserDefault getStringValue:PREFKEY_USERNAME];
    
}

+ (void) setuserIdx : (int) idx {
    
    [UserDefault setIntValue:PREFKEY_USERIDX value:idx];
    
}
+ (int) getUserIdx {
    
    return [UserDefault getIntValue:PREFKEY_USERIDX];
    
}

+ (void) setUserEmail : (NSString *) email {
    
    [UserDefault setStringValue:PREFKEY_USEREMAIL value:email];
    
}
+ (NSString *) getUserEmail {
    
    return [UserDefault getStringValue:PREFKEY_USEREMAIL];
    
}

+ (void) setUserPassword : (NSString *) password {
    
    [UserDefault setStringValue:PREFKEY_USERPASSWORD value:password];
    
}

+ (NSString *) getUserPassword {
    
    return [UserDefault getStringValue:PREFKEY_USERPASSWORD];
    
}

+ (void) setXmppId:(int)idx {
    
    [UserDefault setIntValue:PREFKEY_XMPPID value:idx];
}

+ (int) getXmppId {
    
    return [UserDefault getIntValue:PREFKEY_XMPPID];
}

+ (void) setLastRegisterEmail : (NSString *) email {
    
    [UserDefault setStringValue:PREFKEY_LASTLOGINEMAIL value:email];
    
}

+ (NSString *) getLastRegisterEmail {
    
    return [UserDefault getStringValue:PREFKEY_LASTLOGINEMAIL];
    
}

+ (void) saveDeviceToken:(NSString *) deviceToken {
    
    [UserDefault setStringValue:PREFKEY_DEVICE_TOKEN value:deviceToken];
}

+ (NSString *) getDeviceToken {
    
    return [UserDefault getStringValue:PREFKEY_DEVICE_TOKEN];
}

+ (BOOL) getLogin {
    return [UserDefault getBoolValue:PREFKEY_LOGGED_IN];
}

+ (void) setLogin:(BOOL) isLoggedIn {
    
    [UserDefault setBoolValue:PREFKEY_LOGGED_IN value:isLoggedIn];
}

+ (void) vibrate {
    
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
}

@end

