//
//  ReqConst.h
//  Thunder
//
//  Created by Developer on 12/6/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#ifndef ReqConst_h
#define ReqConst_h


/**
 **     Server URL Macro
 **/
#pragma mark -
#pragma mark - Server URL

#define XMPP_RESOURCE                   @"Thunder"

#define XMPP_SERVER_URL                 @"52.220.171.183"

#define SERVER_BASE_URL                 @"http://52.220.171.183"

#define SERVER_URL [NSString stringWithFormat:@"%@%@", SERVER_BASE_URL, @"/index.php/api/"]

#define REQ_SIGNUP                      @"signup"
#define REQ_LOGIN                       @"login"
#define REQ_LOGOUT                      @"logout"
#define REQ_UPLOADIMAGE                 @"uploadImage"
#define REQ_UPLOADFILE                  @"uploadFile"
#define REQ_ADDFRIENDALLOW              @"addFriendAllow"
#define REQ_SEARCHFRIEND                @"searchFriend"
#define REQ_ADDFRIENDBYID               @"addFriendById"
#define REQ_REJECTFRIENDBYID            @"rejectFriendById"
#define REQ_GETFRIENDLIST               @"getFriendList"
#define REQ_BLOCKFRIEND                 @"blockFriend"
#define REQ_GETRECOMMENDFRIENDS         @"getRecommendFriends"
#define REQ_GETROOMINFO                 @"getRoomInfo"
#define REQ_REGISTERTOKEN               @"registerToken"
#define REQ_GETROOMINFOLIST             @"getRoomInfoList"
#define REQ_ADDFRIENDLIST               @"addFriendList"
#define REQ_BLOCKFRIENDLIST             @"blockFriendList"
#define REQ_SAVETIMELINE                @"saveTimeLine"
#define REQ_SENDPUSH                    @"sendPush"
#define REQ_SAVETEXTTIMELINE            @"saveTextTimeline"
#define REQ_GETTIMELINEWITHDETAIL       @"getTimelineWithDetail"
#define REQ_GETTIMELINEDETAIL           @"getTimelineDetail"
#define REQ_DELMYTIMELINE               @"deleteMyTimeline"
#define REQ_GETMYTIMELINEWITHDETAIL     @"getMyTimeLineWithDetail"
#define REQ_UPDATENICKSTATE             @"updateNickState"
#define REQ_UPDATEANONYMOUSSTATE        @"updateAnonymousState"


/**
 **     Response Params
 **/
#pragma mark -
#pragma mark - Response Params

#define RES_CODE                        @"result_code"

#define RES_IDX                         @"idx"
#define RES_USERINFO                    @"user_info"
#define RES_USERNAME                    @"username"
#define RES_EMAIL                       @"email"
#define RES_PHOTOURL                    @"photo_url"
#define RES_ALLOWFRIEND                 @"allow_friend"
#define RES_NICKNAME                    @"nickname"
#define RES_NICKPHOTO                   @"nickphoto"
#define RES_NICKSTATE                   @"nickstate"
#define RES_ANONYNAME                   @"anonymous_name"
#define RES_ANONYSTATE                  @"anonymous_state"
#define RES_IS_TIMELINE_PUBLISH         @"is_timeline_public"
#define RES_RECOMMEND_COUNT             @"recommand_count"

#define RES_FRIENDLIST                  @"friend_list"
#define RES_ID                          @"id"
#define RES_BLOCK_STATUS                @"block_status"
#define RES_REQUEST_STATUS              @"request_status"

#define RES_USERLIST                    @"user_list"
#define RES_LABEL                       @"label"

//
#define RES_COUNTRY                     @"country"
#define RES_NAME                        @"name"
#define RES_CITY                        @"city"
#define RES_COUNTRYID                   @"country_id"
//
#define RES_ITEM_INFOS                  @"item_infos"
#define RES_DESCRIPTION                 @"description"
#define RES_PIC_URL                     @"picture_url"
#define RES_FOUNDED                     @"founded"
#define RES_USERID                      @"user_id"
#define RES_FOLLOWME                    @"follow_me"
//
#define RES_COMMENTINFOS                @"comment_infos"
#define RES_USERNAME                    @"username"
#define RES_COMMENT                     @"comment"
#define RES_LATITUDE                    @"latitude"
#define RES_LONGITUDE                   @"longitude"
#define RES_LIKE                        @"like"


//Request params

#define PARAM_ID                        @"id"
#define PARAM_IMAGETYPE                 @"image_type"

#define PARAM_NAME                      @"name"
#define PARAM_DESCRIPTION               @"description"
#define PARAM_CARTEGORY_ID              @"category_id"
#define PARAM_COUNTRY                   @"country"
#define PARAM_CITY                      @"city"

//#define PARAM_ID                        @"id"
//#define PARAM_COMMENT                   @"comment"
//#define PARAM_FILE                      @"file"



/**
 **     Response Code
 **/

#pragma mark -
#pragma mark - Response Code

#define CODE_SUCCESS                      0
#define CODE_UNREGISTERED_USER            101
#define CODE_WRONG_PASSWORD               102
#define CODE_USERID_EXIST                 104
#define CODE_USEREMAILEXIST               105

#define CODE_UPLOADFAIL                     111

#endif /* ReqConst_h */
