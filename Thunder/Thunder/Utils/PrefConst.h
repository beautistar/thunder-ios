//
//  PrefConst.h
//  Thunder
//
//  Created by Developer on 12/25/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#ifndef PrefConst_h
#define PrefConst_h

#define PREFKEY_USERID                      @"userID"
#define PREFKEY_USERIDX                      @"useridx"
#define PREFKEY_USEREMAIL                   @"user_email"
#define PREFKEY_USERPASSWORD                @"user_password"
#define PREFKEY_USERNAME                    @"user_name"
#define PREFKEY_LASTLOGINEMAIL              @"last_login_email"
#define PREFKEY_XMPPID                      @"xmpp_id"
#define PREFKEY_DEVICE_TOKEN                @"device_token"
#define PREFKEY_LOGGED_IN                   @"is_loggedin"

#endif /* PrefConst_h */
