//
//  CommonUtils.h
//  Thunder
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Const.h"
#import "AppDelegate.h"
#import "ReqConst.h"
#import "UIImageResizeMagick.h"
#import "NSString+Encode.h"
#import "UserDefault.h"
#import "DBManager.h"
#import "UserEntity.h"
#import "PrefConst.h"
@import AFNetworking;

@interface CommonUtils : NSObject

+ (BOOL) isValidEmail: (NSString *) email;

// save image to file with size specification
+ (NSString *) saveToFile:(UIImage *) srcImage isProfile:(BOOL) isProfile;

+ (void) saveUserInfo;
+ (void) loadUserInfo;

+ (void) setUserID : (NSString *) ID;
+ (NSString *) getUserID;

+ (void) setUserName : (NSString *) username;
+ (NSString *) getUserName;

+ (void) setuserIdx : (int) idx;
+ (int) getUserIdx;

+ (void) setUserEmail : (NSString *) email;
+ (NSString *) getUserEmail;

+ (void) setUserPassword : (NSString *) password;
+ (NSString *) getUserPassword;

+ (void) setXmppId:(int)idx;
+ (int) getXmppId;

+ (void) setLastRegisterEmail : (NSString *) email;
+ (NSString *) getLastRegisterEmail;

+ (void) saveDeviceToken:(NSString *) deviceToken;
+ (NSString *) getDeviceToken;

+ (void) vibrate;

+ (BOOL) getLogin ;
+ (void) setLogin:(BOOL) isLoggedIn;

+ (void) createUploadFolder;
+ (void) createDownloadFolder;

@end
