//
//  DBManager.h
//  Thunder
//
//  Created by Developer on 12/25/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DBManager : NSObject {
    
     NSString * databasePath;
}

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

@property (nonatomic, strong) NSMutableArray *arrResults;

@property (nonatomic, strong) NSMutableArray *arrColumnNames;

@property (nonatomic) int affectedRows;

@property (nonatomic) long long lastInsertedRowID;

+ (DBManager *) getSharedInstance;

- (void) clearDB;

- (NSArray *) loadRoom;

- (void) saveRoomWithName:(NSString *) name participant_name:(NSString *) participantName recent_message:(NSString *)recentMessage  recent_time:(NSString *)recentTime recent_counter:(int) recentCounter;

- (void) updateRoomWithName:(NSString *) name participant_name:(NSString *)participantsName recentMessage:(NSString *)recentMessage recentTime:(NSString *) recentTime recetCounter:(int) recentCounter;

- (void) updateRoomWithName:(NSString *) name recentCounter:(int) recentCounter;

- (void) updateRoomWithName:(NSString *)name participant_name:(NSString *)participantsName;

- (void) removeRoomWithName:(NSString *) name;

////////////////////////////////////////////////////
//// Save & Load Chat List from(to) DB
////////////////////////////////////////////////////

- (void) saveChatWithRoomName:(NSString *) roomName content:(NSString *) message from:(int) sender date:(NSString *) datetime;
- (void) saveChatWithRoomName:(NSString *) roomName content:(NSString *) message from:(int) sender date:(NSString *) datetime current:(int) isCurrent;
- (NSMutableArray *) loadMessageWithRoomName:(NSString *) roomName refreshCount:(int) refreshCount;
- (void) updateChatNoCurrentWithRoomName:(NSString *) roomName;
- (void) updateChatNoCurrent;

- (void) addBlockFriend:(int) friend_id;
- (void) deleteBlockFriend:(int) friend_id;
@end
