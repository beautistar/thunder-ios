//
//  MyPostViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "MyPostViewController.h"
#import "UITextView+Placeholder.h"
#import "MyPostCell.h"
#import "CommonUtils.h"

@interface MyPostViewController () <UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    
    __weak IBOutlet UICollectionView *cvMyPost;
    NSArray *arrayOfImages;
    int pos;
    __weak IBOutlet UITextView *tvDescription;
    
    NSString *photoPath;
    
}

@end

@implementation MyPostViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tvDescription.placeholder = @"Please input description here.";
    
    UICollectionViewFlowLayout *collectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
    [collectionViewFlowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    cvMyPost.collectionViewLayout = collectionViewFlowLayout;
    
    cvMyPost.pagingEnabled = NO;
    
    arrayOfImages = [[NSArray alloc] initWithObjects:
                     @"model",@"model",@"model",@"model",
                     @"model",@"model",@"model",@"model",
                     @"model",@"model",@"model",@"model",
                     nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - collection View

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
//    return 5;
    return [arrayOfImages count];
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath

{
    
    static NSString *CellIdentifier = @"MyPostCell";
    MyPostCell *cell = (MyPostCell *) [collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];

    cell.imvPicture.image = [UIImage imageNamed:[arrayOfImages objectAtIndex:indexPath.item]];
    
    if (indexPath.row % 2 == 0) {
        
        [cell.btnVideoPlay setHidden:YES];
    } else {
        
        [cell.btnVideoPlay setHidden:NO];
    }
    
    
    return cell;
    
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(nonnull UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    //top, left, bottom, right
    return UIEdgeInsetsZero;
    
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    
    return 5;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.f;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CGFloat cellWidth = cvMyPost.frame.size.width / 3.0f - 5;
    
    return CGSizeMake( cellWidth, cellWidth);
//    return CGSizeMake( 160, 160);
    
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // pos represents previous or initial position
    // indexPath represents clicked cell position
    
    if(indexPath.row > pos)
    {
        if (indexPath.row - pos == 2) {
            pos = pos + 1;
        }
        [self changeDate];
    }
    else if (indexPath.row == pos)
    {
        NSLog(@"Do Nothing");
    }
    else
    {
        if (indexPath.row + 2 == pos) {
            pos = pos - 1;
        }
        [self changeDate1];
    }
    //NSLog(@"%@",[arrDate objectAtIndex:indexPath.row]);
}

-(void)changeDate
{
    if (pos<(arrayOfImages.count - 2)) {
        pos=pos+1;
        [cvMyPost selectItemAtIndexPath:[NSIndexPath indexPathForRow:pos inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        NSLog(@"%@",[arrayOfImages objectAtIndex:pos]);
        //self.lblMovieName.text =[arrayOfImages objectAtIndex:pos];
    }
}
-(void)changeDate1
{
    if(pos>2)
    {
        pos=pos-1;
        [cvMyPost selectItemAtIndexPath:[NSIndexPath indexPathForRow:pos inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
        NSLog(@"%@",[arrayOfImages objectAtIndex:pos]);
        //self.lblMovieName.text =[arrayOfImages objectAtIndex:pos];
    }
    else{
        
    }
}

#pragma mark - imagedelegate

- (IBAction)addProfile:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self camera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gallery];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    //actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}


- (void) camera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) gallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:NO];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
//                [imvProfile setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
            });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

@end
