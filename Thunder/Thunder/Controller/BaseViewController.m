//
//  BaseViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD.h"
#import "Toaster.h"

@interface BaseViewController () {
    
    MBProgressHUD *_hud;
    
    MBProgressHUD *toastHUD;
}

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
}

- (void) viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}



#pragma mark - MBProgressHUD

- (void) showLoadingViewWithTitle:(NSString *) title;
{
    
    _hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // Set the hud to display with a color
    _hud.color = [UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:0.0];
    
    _hud.labelText = title;
    
}

- (void) showLoadingView {
    
    [self showLoadingViewWithTitle:nil];
}

- (void) hideLoadingView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    _hud = nil;
}

- (void) hideLoadingView : (NSTimeInterval) delay {
    [_hud hide:YES afterDelay:delay];
    _hud = nil;
}

#pragma mark - Show Alert

- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative {
    
    NSDictionary *pinkBoldAttribtes = @{NSForegroundColorAttributeName :[UIColor colorWithRed:2/255.0 green:136/255.0 blue:209/255.0 alpha:1.0], NSFontAttributeName: [UIFont fontWithName:@"Helvetica-Bold" size:18.0]};
    
    NSMutableAttributedString *attributedTitle;
    
    if (title != nil) {
        attributedTitle = [[NSMutableAttributedString alloc] initWithString:title];
        [attributedTitle addAttributes:pinkBoldAttribtes range:NSMakeRange(0, title.length)];
    }
    
    
    NSMutableAttributedString *attributedMessage = [[NSMutableAttributedString alloc] initWithString:message];
    [attributedMessage addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:16.0] range:NSMakeRange(0, message.length)];
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle:title
                                 message:message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    if(title != nil) {
        [alert setValue:attributedTitle forKey:@"attributedTitle"];
    }
    
    [alert setValue:attributedMessage forKey:@"attributedMessage"];
    
    if(strPositivie != nil) {
        UIAlertAction * yesButton = [UIAlertAction
                                     actionWithTitle:strPositivie
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action)
                                     {
                                         //Handel your yes please button action here
                                         [alert dismissViewControllerAnimated:YES completion:nil];
                                     }];
        
        [alert addAction:yesButton];
    }
    
    if(strNegative != nil) {
        UIAlertAction * noButton = [UIAlertAction
                                    actionWithTitle:strNegative
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                    }];
        
        [alert addAction:noButton];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    //    alert.view.tintColor = [UIColor darkGrayColor];
    //    alert.view.backgroundColor = [UIColor colorWithRed:71/255.0 green:74/255.0 blue:85/255.0 alpha:1.0];
}

- (void)showToastMessage:(NSString *)message {
    
    [[Toast makeText:message] show];
    
//    if(toastHUD == nil ) {
//        
//        toastHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        toastHUD.color = [UIColor colorWithRed:0.23 green:0.5 blue:0.82 alpha:0.7];
//        
//        // Configure for text only and offset down
//        toastHUD.mode = MBProgressHUDModeText;
//        toastHUD.detailsLabelText = message;
//        toastHUD.margin = 10.0f;
//        toastHUD.yOffset = 150.0f;
//        toastHUD.removeFromSuperViewOnHide = YES;
//        [toastHUD hide:YES afterDelay:1.5f];
//        
//    } else {
//        
//        [toastHUD hide:YES];
//        toastHUD = nil;
//        
//        toastHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        toastHUD.color = [UIColor colorWithRed:0.23 green:0.5 blue:0.82 alpha:0.7];
//        
//        // Configure for text only and offset down
//        toastHUD.mode = MBProgressHUDModeText;
//        toastHUD.detailsLabelText = message;
//        toastHUD.margin = 10.0f;
//        toastHUD.yOffset = 150.0f;
//        toastHUD.removeFromSuperViewOnHide = YES;
//        [toastHUD hide:YES afterDelay:1.5f];
//    }
}

@end
