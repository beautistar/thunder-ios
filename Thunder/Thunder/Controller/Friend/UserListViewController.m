//
//  UserListViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "UserListViewController.h"
#import "FriendListCell.h"
#import "CommonUtils.h"



@interface UserListViewController () <UITableViewDataSource, UITableViewDelegate, FriendCellDelegate> {
    
    __weak IBOutlet UITableView *tblUserList;
    
    UserEntity *_user;
    
    NSMutableArray *_userList;
}

@end

@implementation UserListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    _userList = [[NSMutableArray alloc] init];
    tblUserList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self getRecommendFriend];
}

- (void) getRecommendFriend {
    
    [self showLoadingView];
    
    [_userList removeAllObjects];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETRECOMMENDFRIENDS,  _user._idx];
    
    NSLog(@"recommend friends request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"recommend friends response url : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            
            NSArray * arrFriend = [responseObject valueForKey:RES_USERLIST];
            
            for (NSDictionary * _dict in arrFriend) {
                
                FriendEntity *_friend = [[FriendEntity alloc] init];
                
                _friend._idx = [[_dict valueForKey:RES_ID] intValue];
                _friend._name = [_dict valueForKey:RES_NAME];
                _friend._photoUrl = [_dict valueForKey:RES_PHOTOURL];
                _friend._blockStatus = [[_dict valueForKey:RES_LABEL] intValue];
                _friend._requestState = 1;
                
                [_userList addObject:_friend];
            }

            [self hideLoadingView];
            
            [tblUserList reloadData];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
    
}

- (IBAction)closeAction:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _userList.count;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.f;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FriendListCell *cell = (FriendListCell *) [tableView dequeueReusableCellWithIdentifier:@"FriendListCell"];
    
    FriendEntity *_friend = (FriendEntity *) _userList[indexPath.row];
    cell.delegate = self;
    
    [cell setUser:_friend];
    
    return cell;
}

- (void) addFriend:(FriendEntity *)_friend {
    
    [self showLoadingView];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_ADDFRIENDBYID,  _user._idx, _friend._idx];
    
    NSLog(@"add friends request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"add friends response url : %@", responseObject);
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [_userList removeObject:_friend];
            [_user._friendList addObject:_friend];
            if (_friend._requestState == 1) {
                [self showToastMessage:SUCCESS_ADDED];
            } else {
                [self showToastMessage:SUCCESS_REQUEST];
            }
            
            
//            working
//            sendpush
//            after send push, discount the recomment count of user
            _user._recommandCounter--;
            
            [tblUserList reloadData];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
    
}

- (void) rejectFriend:(FriendEntity *)_friend {
    
    [self showLoadingView];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d", SERVER_URL, REQ_REJECTFRIENDBYID,  _user._idx, _friend._idx];
    
    NSLog(@"reject friends request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"reject friends response url : %@", responseObject);
        
        
        [self hideLoadingView];
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [_userList removeObject:_friend];

            [self showToastMessage:SUCCESS_REJECT];
            
//            working
//            sendpush
//            after send push, discount the recomment count of user
            
            _user._recommandCounter--;
            
            [tblUserList reloadData];

        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
}




@end
