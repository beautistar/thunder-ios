//
//  FriendListViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "FriendListViewController.h"
#import "UserListViewController.h"

#import "FriendListCell.h"
#import "CommonUtils.h"


@interface FriendListViewController () <UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *tblFriendList;
    __weak IBOutlet UIImageView *imvMyProfile;
    __weak IBOutlet UILabel *lblName;
    __weak IBOutlet UILabel *lblAddUnread;
    
    UserEntity *_user;
    
    NSMutableArray * _arrResults;
}

@end

@implementation FriendListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (void) setupView {
    
    tblFriendList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tblFriendList.allowsMultipleSelectionDuringEditing = NO;
    
    APPDELEGATE.gTabbar = self.tabBarController.tabBar;
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
//    if(lastSelectedTapIdx != 0) {
//        self.tabBarController.selectedIndex = lastSelectedTapIdx;
//        lastSelectedTapIdx = 0;
//    }
    
    _user = APPDELEGATE.Me;
    
    [APPDELEGATE notifyReceiveNewMessage];
    
    [self getFriends];
    
    [self initView];
    
}

- (void) getFriends {
    
    if(_arrResults == nil) {
        _arrResults = [NSMutableArray array];
    } else {
        [_arrResults removeAllObjects];    }
    
    for(int i = 0; i < [_user._friendList count]; i ++) {
        
        FriendEntity *_entity = (FriendEntity *)_user._friendList[i];
        
        if(_entity._blockStatus == 1)               // unblocked user
            [_arrResults addObject:_entity];
    }
    
    NSArray *sortedArray;
    sortedArray = [_arrResults sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
        NSString *first = [(FriendEntity*)a _name];
        NSString *second = [(FriendEntity*)b _name];
        return [first compare:second];
    }];
    
    [_arrResults removeAllObjects];
    [_arrResults addObjectsFromArray:sortedArray];
}

- (void) initView {
    
    // 유저정보설정
    [imvMyProfile setImageWithURL:[NSURL URLWithString:_user._photoUrl] placeholderImage:[UIImage imageNamed:@"bg_non_profile"]];
    lblName.text = _user._name;
    
    // setup unread info
    if (_user._recommandCounter == 0) {
        
        [lblAddUnread setHidden:YES];
    } else {
        lblAddUnread.text = [NSString stringWithFormat:@"%d", _user._recommandCounter];
        [lblAddUnread setHidden:NO];
    }
    
    [tblFriendList reloadData];
}

- (void) viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


- (IBAction)inviteAction:(id)sender {
    
    NSMutableArray *sharingItems = [NSMutableArray new];
    
//    if (1) {
//        [sharingItems addObject:@""];
//    }
//    if (2) {
//        [sharingItems addObject:[UIImage imageNamed:@"model"]];
//    }
//    if (3) {
        [sharingItems addObject:[NSURL URLWithString:@"http://www.apple.com"]];
//    }
    
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
    [self presentViewController:activityController animated:YES completion:nil];

}

- (IBAction)addAction:(id)sender {
    
//    UserListViewController *modalVC = (UserListViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"UserListViewController"];
//    
//    self presentationController
    
}


#pragma mark - tableview delegate & datasource

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _arrResults.count;

}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FriendListCell *cell = (FriendListCell *) [tableView dequeueReusableCellWithIdentifier:@"FriendListCell"];
    
    FriendEntity * _entity = (FriendEntity *) _arrResults[indexPath.row];
    
    cell.lblName.text = _entity._name;
    
    if (_entity._requestState == 1) {
        cell.lblState.text = @"Friend";
    } else {
        cell.lblState.text = @"Waiting";
    }
    
    [cell.imvPhoto setImageWithURL:[NSURL URLWithString:_entity._photoUrl] placeholderImage:[UIImage imageNamed:@"bg_non_profile"]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewRowAction *block = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"Block" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                     {
                                         FriendEntity * blockUser = _arrResults[indexPath.row];
                                         [self blockFriend:blockUser];
                                         NSLog(@"block user id : %d", blockUser._idx);
//                                         NSLog(@"block user id : %ld", (long)indexPath.row);
                                     }];
    
    return @[block];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FriendEntity * _entity = (FriendEntity *) _arrResults[indexPath.row];
    
    if (_entity._requestState == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:ALERT_WAITING positive:nil negative:ALERT_OK];
    } else {
        
        [self gotoChatVC:_entity];
    }
}


- (void) blockFriend:(FriendEntity *) blockUser {
    
    [self showLoadingView];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d/%d/%d", SERVER_URL, REQ_BLOCKFRIEND,  _user._idx, blockUser._idx, 0];
    
    NSLog(@"blockfriend request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"blockfriend response url : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            [self hideLoadingView];
            
            // block selected friend
            blockUser._blockStatus = 0;
            
            for(FriendEntity * _friend in _user._friendList) {
                if(_friend._idx == blockUser._idx) {
                    _friend._blockStatus = 0;
                    return;
                }
            }
            
//            [[DBManager getSharedInstance] addBlockFriend:blockUser._idx];
            
            [_user._friendList addObject:blockUser];
            
//            [self getFriends];
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
}

- (void) gotoChatVC:(FriendEntity*) friend {
    
//    NSMutableArray *participants = [[NSMutableArray alloc] init];
//    [participants addObject:friend];
//    RoomEntity *room = [[RoomEntity alloc] init];
//    room._ownerIdx = _user._idx;
//    room._groupPhoto = friend._photoUrl;
//    room._groupName = friend._name;
//    room._ownerState = 0;
//    [room makeRoolRoom];
//    
//    
//    if (![_user._roomList containsObject:room]) {
//        
//        [_user._roomList addObject:room];
//        
//    }
//    
//    [APPDELEGATE.xmpp EnterChattingRoom:room];
//    
//    NSLog(@"%d", friend._idx);
    
    
    // go to ChatViewController
    [self makeRoomWithFriend];
    [APPDELEGATE.xmpp EnterChattingRoom:_chatRoom];
    
    //    [self performSegueWithIdentifier:SEGUE_FRIENDPROFILE_CHAT sender:self];
    
    // Tapbar controller
    UITabBarController * mainTab = (UITabBarController*) [self presentingViewController];
    //    mainTab.tabBarController.selectedIndex = 1; // ChatView
    
    //lastSelectedTapIdx = 1;
    
    UINavigationController * nav = (UINavigationController *)[self.storyboard instantiateViewControllerWithIdentifier:@"chattingNav"];
    nav.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    nav.modalPresentationStyle = UIModalPresentationOverFullScreen;
    
    ChatViewController *  chatVC = (ChatViewController *) nav.topViewController;
    chatVC.chattingRoom = _chatRoom;
    chatVC.from = FROM_PROFILEVIEWCONTROLLER;   // go to ChatViewController from FriendProfileViewController
    // 방에 들어갈때...
    // 읽지 않은 메시지수를 초기화
    if(_chatRoom._recentCounter > 0) {
        
        _user.notReadCount -= _chatRoom._recentCounter;
        [APPDELEGATE notifyReceiveNewMessage];
        
        _chatRoom._recentCounter = 0;
        // update db as _chatRoom._recentCounter = 0....(only this field)
        [[DBManager getSharedInstance] updateRoomWithName:_chatRoom._name recentCounter:0];
    }
    
    [self dismissViewControllerAnimated:NO completion:nil];
    [mainTab presentViewController:nav animated:NO completion:nil];
}

- (void) makeRoomWithFriend {
    
    // 선택된 친구와의 대화방을 만든다...
    NSMutableArray *_friends = [NSMutableArray array];
    [_friends addObject:chatFriend];
    
    // 대화방...
    _chatRoom = [[RoomEntity alloc] initWithParticipants:_friends];
    
    // 만들어진 방이 이미 존재하는 방(유저가 가지고 있는 방)인가를 검사
    for(RoomEntity * _existEntity in _user._roomList) {
        
        if([_existEntity equals:_chatRoom]) {
            _chatRoom = _existEntity;
            return;
        }
    }
    
    [_user._roomList addObject:_chatRoom];
    
    [[DBManager getSharedInstance] saveRoomWithName:_chatRoom._name participant_name:_chatRoom._participantsName recent_message:_chatRoom._recentContent recent_time:_chatRoom._recentDate recent_counter:_chatRoom._recentCounter];
}


@end
