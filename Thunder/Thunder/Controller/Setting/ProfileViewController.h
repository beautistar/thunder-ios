//
//  ProfileViewController.h
//  Thunder
//
//  Created by Developer on 11/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface ProfileViewController : UIViewController

@property (nonatomic, strong) UserEntity *_user;
@property (nonatomic) int _from;
@end
