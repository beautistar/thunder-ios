//
//  RoleSaveViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "RoleSaveViewController.h"
#import "Const.h"

@interface RoleSaveViewController () {
    
}


@end

@implementation RoleSaveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    [self initLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self initLoad];
}

- (void) initLoad {
    
    if (self._from == FOR_NICKNAME) {
        
        self.navigationItem.title = @"NickName";
        _tfName.placeholder = @"Please input NickName";
        
    } else {
        
        self.navigationItem.title = @"Anonymous";
        _tfName.placeholder = @"Please input Anonymous name";
        
    }
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)saveAction:(id)sender {
    
    if (_tfName.text.length != 0) {
    
        [self performSegueWithIdentifier:@"SegueFromSave" sender:self];
    } else {
        
        [self showAlertDialog:ALERT_TITLE message:@"Please input name" positive:ALERT_OK negative:nil];
    }
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    return 250;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
