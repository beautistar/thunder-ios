//
//  RoleViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "RoleViewController.h"
#import "RoleSaveViewController.h"
#import "Const.h"

@interface RoleViewController () {
    
    __weak IBOutlet UILabel *lblNickName;
    __weak IBOutlet UILabel *lblAnonymous;
    
}

@end

@implementation RoleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    RoleSaveViewController *destVC = [segue destinationViewController];
    
    if ([segue.identifier isEqualToString:@"SegueForNickName"]) {
        
        destVC._from = FOR_NICKNAME;
        
    } else {
        
        destVC._from = FOR_ANONYMOUS;
    }
}

- (IBAction) unwindFromSave:(UIStoryboardSegue *) unwindSegue {
    
    RoleSaveViewController *sourceVC = unwindSegue.sourceViewController;
    
    NSString *name = @"No define";
    if (sourceVC.tfName.text.length != 0) {
        
        name = sourceVC.tfName.text;
    }
    
    if (sourceVC._from == FOR_NICKNAME) {
        
        lblNickName.text = [NSString stringWithFormat:@"NickName (%@)", name];
    } else {
        
        lblAnonymous.text = [NSString stringWithFormat:@"NickName (%@)", name];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
