//
//  LoginViewController.m
//  Thunder
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "LoginViewController.h"
#import "CommonUtils.h"

@interface LoginViewController () {
    
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPassword;
    
    UserEntity * _user;
    int _nReqCounter;
    
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    //for test
    //[self gotoMain];
    //bAutoLogin = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [CommonUtils loadUserInfo];
    
    if (_user._email.length != 0) {
        
        tfEmail.text = _user._email;
        tfPassword.text = _user._password;
    }
    
    if (bAutoLogin) {
        
        [self doLogin];
    }
    
//    if (fromLogout) {
//        
//        tfEmail.text = @"";
//        tfPassword.text = @"";
//    }
    
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (IBAction)loginAction:(id)sender {
    
    if ([self isValid]) {
        
        [self doLogin];
    }
}

- (BOOL) isValid {
    
    if (tfEmail.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_USEREMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (![CommonUtils isValidEmail:tfEmail.text]) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_VALIDEMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfPassword.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_PASSWORD positive:nil negative:ALERT_OK];
        return NO;
    }
    return YES;
}

- (void) doLogin {
    
    [self showLoadingView];
    
    NSString *email = [tfEmail.text encodeString:NSUTF8StringEncoding];
    
    NSString * password = [[tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] encodeString:NSUTF8StringEncoding];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@", SERVER_URL, REQ_LOGIN,  email, password];
    
    NSLog(@"login request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"login response url : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            _user._idx = [[responseObject valueForKey:RES_IDX] intValue];
            _user._userName = [responseObject valueForKey:RES_USERNAME];
            _user._name = [responseObject valueForKey:RES_NAME];
            _user._email = [responseObject valueForKey:RES_EMAIL];
            _user._photoUrl = [responseObject valueForKey:RES_PHOTOURL];
            _user._password = [tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            _user._allowFriend = [[responseObject valueForKey:RES_ALLOWFRIEND] intValue];
            _user._nickName = [responseObject valueForKey:RES_NICKNAME];
            _user._nickPhoto = [responseObject valueForKey:RES_NICKPHOTO];
            _user._nickState = [[responseObject valueForKey:RES_NICKSTATE] intValue];
            _user._anonymousName = [responseObject valueForKey:RES_ANONYNAME];
            _user._anonymousState = [[responseObject valueForKey:RES_ANONYSTATE] intValue];
            _user._isPublicTimeline = [[responseObject valueForKey:RES_IS_TIMELINE_PUBLISH] intValue];
            _user._recommandCounter = [[responseObject valueForKey:RES_RECOMMEND_COUNT] intValue];
            
            [CommonUtils setUserEmail:tfEmail.text];
            [CommonUtils setUserPassword:tfPassword.text];
            [CommonUtils setLogin:YES];
            [CommonUtils setXmppId:_user._idx];            
            
            
            // If new user, clear DB
            if([CommonUtils getLastRegisterEmail] == nil || ![[CommonUtils getLastRegisterEmail] isEqualToString:email]) {
                
                [CommonUtils setLastRegisterEmail:email];
                [[DBManager getSharedInstance] clearDB];
            }
            
            [self registerToken];
            
        } else if (result_code == CODE_UNREGISTERED_USER) {
            
            [self hideLoadingView];
            
            [self showAlertDialog:nil message:UNREGISTERED_USER positive:ALERT_OK negative:nil];
            
        } else if (result_code == CODE_WRONG_PASSWORD) {
            
            [self hideLoadingView];
            
            [self showAlertDialog:nil message:WRONG_PASSWORD positive:ALERT_OK negative:nil];
            
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];    
}

- (void) registerToken {
    
    NSString * token = [CommonUtils getDeviceToken];
    
    if (token.length == 0) {
        
        [self getFriendList];
        return;
    }
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_REGISTERTOKEN, token];
    
    NSLog(@"register token request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"register token response url : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            NSLog(@"register token succeeded");
        }
        
        [self getFriendList];
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
}

- (void) getFriendList {
    
    if(_user._friendList != nil)
        [_user._friendList removeAllObjects];
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETFRIENDLIST, _user._idx];
    
    NSLog(@"get friendlist request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"get friend list response url : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            NSArray * arrFriend = [responseObject valueForKey:RES_FRIENDLIST];
            
            for (NSDictionary * _dict in arrFriend) {
                
                FriendEntity *_friend = [[FriendEntity alloc] init];
                
                _friend._idx = [[_dict valueForKey:RES_ID] intValue];
                _friend._name = [_dict valueForKey:RES_NAME];
                _friend._photoUrl = [_dict valueForKey:RES_PHOTOURL];
                _friend._blockStatus = [[_dict valueForKey:RES_BLOCK_STATUS] intValue];
                _friend._isFriend = YES;
                _friend._requestState = [[_dict valueForKey:RES_REQUEST_STATUS] intValue];
                
                [_user._friendList addObject:_friend];
            }
//
            if ([APPDELEGATE.xmpp connect:_user._idx password:_user._password]) {
                
                [self loadRoomList];
                
                // xmpp 서버에 로그인 성공 - Friend창으로 이동
                //[self gotoFriendView];
            }
            else
            {
                // xmpp 서버에 로그인 실패, 다시 로그인 시도
                [self hideLoadingView];
                [self showToastMessage:NSLocalizedString(@"ALERT_CONNECT_ERROR", nil)];
                
            }
        }
        
        else {
            
            [self showToastMessage:NSLocalizedString(@"ALERT_CONNECT_ERROR", nil)];
            
            if ([APPDELEGATE.xmpp connect:_user._idx password:_user._password])
            {
                [self loadRoomList];
                // xmpp 서버에 로그인 성공 - Friend창으로 이동
                //[self gotoFriendView];
                
            } else
            {
                // xmpp 서버에 로그인 실패, 다시 로그인 시도
                [self hideLoadingView];
                [self showToastMessage:NSLocalizedString(@"ALERT_CONNECT_ERROR", nil)];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
    
}

- (void) loadRoomList {
    
    _user._readNotCount = 0;
    
    // initialize user's room list
    if(_user._roomList != nil)
        [_user._roomList removeAllObjects];
    
    NSArray * _arrRooms = [[DBManager getSharedInstance] loadRoom];
    
    _nReqCounter = (int)[_arrRooms count];
    
    if(_nReqCounter == 0) {
        
        [self hideLoadingView];
        
        /////////////////////////
        [self gotoMain];
        
        return;
    }
    
    for(int i = 0; i < [_arrRooms count]; i ++) {
        
        NSString * name = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"PARTICIPANTNAME"]];
        
        // 방에 참가하는 친구들 --- 정보를 서버에서 가져온다
        NSMutableArray * _arrFriends = [NSMutableArray array];
        // make server url
        NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GETROOMINFOLIST, name];
        
        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            
            if(nResult_Code == CODE_SUCCESS) {
                
                NSMutableArray * _roomParticipants = [responseObject objectForKey:RES_USERINFO];
                
                for (int j = 0; j < [_roomParticipants count]; j ++) {
                    
                    NSDictionary * _dict = _roomParticipants[j];
                    
                    int _participantIdx = [[_dict valueForKey:RES_ID] intValue];
                    
                    if(_user._idx == _participantIdx) continue;
                    
                    // room participants들 가운데 친구들은 친구목록에서 얻어서 추가
                    // 새 유저에 대하여서는 서버에서 응답에 따라.....
                    if([_user getFriend:_participantIdx] != nil) {
                        
                        [_arrFriends addObject:[_user getFriend:_participantIdx]];
                        
                    } else {
                        
                        FriendEntity * _participant = [[FriendEntity alloc] init];
                        
//                        _participant._idx = _participantIdx;
//                        _participant._name = [_dict valueForKey:RES_NAME];
//                        _participant._email = [_dict valueForKey:RES_EMAIL];
//                        _participant._label = [_dict valueForKey:RES_LABEL];
//                        _participant._photoUrl = [_dict valueForKey:RES_PHOTO_URL];
//                        _participant._bgUrl = [_dict valueForKey:RES_BG_URL];
//                        //                        _participant._blockStatus = [[_dict valueForKey:RES_BLOCK_STATUS] intValue];
//                        _participant._phoneNumber = [_dict valueForKey:RES_PHONE_NUMBER];
                        
                        [_arrFriends addObject:_participant];
                    }
                }
                
                // 새로운 방을 만들고....
                // db에 저장된 자료를 가지고 값을 설정한다.
                // recentContent, recenctDate, recentCounter
                NSString * roomName = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"NAME"]];
                NSString * recentContent = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTMESSAGE"]];
                NSString * recentDate = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTTIME"]];
                NSString * recentCounter = [[_arrRooms objectAtIndex:i] objectAtIndex:[[DBManager getSharedInstance].arrColumnNames indexOfObject:@"RECENTCOUNTER"]];
                
                //                RoomEntity * loadRoom = [[RoomEntity alloc] initWithParticipants:_arrFriends];
                
                RoomEntity *loadRoom = [[RoomEntity alloc] initWithName:roomName participants:_arrFriends];
                
                loadRoom._recentContent = recentContent;
                loadRoom._recentTime = recentDate;
                loadRoom._recentCounter = [recentCounter intValue];
                
                _user._readNotCount += loadRoom._recentCounter;
                
                [_user._roomList addObject:loadRoom];
            }
            
            if(--_nReqCounter <= 0) {
                
                [self hideLoadingView];
                //
                //[self gotoFriendView];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            
            if(--_nReqCounter <= 0) {
                
                [self hideLoadingView];
                
                //[self gotoFriendView];
            }
            
            NSLog(@"Error: %@", error);
        }];
    }
    
    
}


- (void) gotoMain {
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MainTabViewController"]];
}

//- (CGFloat) getOffsetYWhenShowKeybarod {
//    
//    return 230;
//}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
