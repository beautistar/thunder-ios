//
//  BaseViewController.h
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import Toaster;

@interface BaseViewController : UIViewController {
    
    
}



- (void) showLoadingView;
- (void) showLoadingViewWithTitle:(NSString *) title;
- (void) hideLoadingView;
- (void) hideLoadingView : (NSTimeInterval) delay ;
- (void) showAlertDialog : (NSString *)title message:(NSString *) message positive:(NSString *)strPositivie negative:(NSString *) strNegative;

- (void)showToastMessage:(NSString *)message;

@end
