//
//  ChatListViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "ChatListViewController.h"
#import "FriendListCell.h"


@interface ChatListViewController () <CustomIOSAlertViewDelegate, UITableViewDelegate, UITableViewDataSource> {
    
    CustomIOSAlertView *_alertView;
    __weak IBOutlet UITableView *tblChatList;
}

@end

@implementation ChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tblChatList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
}

- (void) viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)roleAction:(id)sender {
    
    _alertView = [[CustomIOSAlertView alloc] init];
    _alertView.closeOnTouchUpOutside = true;
    // Add some custom content to the alert view
    [_alertView setContainerView:[self createContentView]];

    [_alertView setDelegate:self];
    
    [_alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [_alertView show];
    
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [_alertView close];
    
}

- (UIView *)createContentView
{
    UIView *alertContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 290, 150 )];
    
    UIButton *btn_realname = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 290, 50)];
    [btn_realname setTitle:@"Real Name" forState:UIControlStateNormal];
    [btn_realname setTag:11];
    [btn_realname addTarget:self action:@selector(selectedRole:) forControlEvents:UIControlEventTouchUpInside];
    [btn_realname setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [alertContentView addSubview:btn_realname];
    
    UIButton *btn_nickname = [[UIButton alloc] initWithFrame:CGRectMake(0, 50, 290, 50)];
    [btn_nickname setTitle:@"Nick Name" forState:UIControlStateNormal];
    [btn_nickname setTag:12];
    [btn_nickname setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_nickname addTarget:self action:@selector(selectedRole:) forControlEvents:UIControlEventTouchUpInside];
    [alertContentView addSubview:btn_nickname];
    
    UIButton *btn_anonymous = [[UIButton alloc] initWithFrame:CGRectMake(0, 100, 290, 50)];
    [btn_anonymous setTitle:@"Anonymous" forState:UIControlStateNormal];
    [btn_anonymous setTag:13];
    [btn_anonymous setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn_anonymous addTarget:self action:@selector(selectedRole:) forControlEvents:UIControlEventTouchUpInside];
    [alertContentView addSubview:btn_anonymous];
    
    return alertContentView;
}

- (IBAction)selectedRole:(id)sender {
    
    UIButton *btn = (UIButton *) sender;
    
    switch (btn.tag) {
        case 11:
            NSLog(@"Role is RealName");
            break;
            
        case 12:
            
            NSLog(@"Role is nickname");
            break;
            
        default:
            
            NSLog(@"Role is Anonymous");
            break;
    }
    
    [_alertView close];
    
}

- (IBAction)addUserAction:(id)sender {
    
}

#pragma mark - tableview datasource & delegate

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 70.f;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FriendListCell *cell = (FriendListCell *) [tableView dequeueReusableCellWithIdentifier:@"FriendListCell"];
    
    return cell;
    
}






@end
