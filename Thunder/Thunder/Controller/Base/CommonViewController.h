//
//  CommonViewController.h
//  Thunder
//
//  Created by Developer on 1/9/17.
//  Copyright © 2017 Developer. All rights reserved.
//

#import "BaseViewController.h"

@interface CommonViewController : BaseViewController

- (CGFloat) getOffsetYWhenShowKeybarod;

@end
