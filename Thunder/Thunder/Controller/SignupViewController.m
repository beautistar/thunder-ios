//
//  SignupViewController.m
//  Thunder
//
//  Created by Developer on 11/17/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "SignupViewController.h"
#import "CommonUtils.h"

@interface SignupViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    
    NSString *photoPath;
    
    __weak IBOutlet UIImageView *imvProfile;
    
    __weak IBOutlet UITextField *tfUserId;
    __weak IBOutlet UITextField *tfUserName;
    __weak IBOutlet UITextField *tfEmail;
    __weak IBOutlet UITextField *tfPassword;
    __weak IBOutlet UITextField *tfConfirmPwd;
    
    UserEntity *_user;
    
}

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _user = APPDELEGATE.Me;
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initView {
    
    imvProfile.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [self.view endEditing:YES];
}

- (IBAction)signupAction:(id)sender {
    
    if ([self isValid]) {
        
        [self doRegister];
    }
}

- (BOOL) isValid {
    
    if(tfUserId.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_USERID positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfUserName.text.length == 0) {
        
        [self showAlertDialog:ALERT_TITLE message:INPUT_USERNAME positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfEmail.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_USEREMAIL positive:nil negative:ALERT_OK];
        return NO;
    }else if (![CommonUtils isValidEmail:tfEmail.text]) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_VALIDEMAIL positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfPassword.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_PASSWORD positive:nil negative:ALERT_OK];
        return NO;
    } else if (tfConfirmPwd.text.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_PASSWORDCFM positive:nil negative:ALERT_OK];
        return NO;
    } else  if (![tfPassword.text isEqualToString:tfConfirmPwd.text]) {
        [self showAlertDialog:ALERT_TITLE message:INPUT_INVALIDPWD positive:nil negative:ALERT_OK];
        return NO;
    } else if (photoPath.length == 0) {
        [self showAlertDialog:ALERT_TITLE message:SELECT_PHOTO positive:nil negative:ALERT_OK];
        return NO;
    }
    
    return YES;
}

- (void) doRegister {
    
    [self showLoadingView];
    
    NSString *userid = [tfUserId.text encodeString:NSUTF8StringEncoding];
    NSString *username = [tfUserName.text encodeString:NSUTF8StringEncoding];
    NSString * password = [[tfPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] encodeString:NSUTF8StringEncoding];
    NSString * email = [tfEmail.text encodeString:NSUTF8StringEncoding];
    
    
    NSString * url = [NSString stringWithFormat:@"%@%@/%@/%@/%@/%@/%d", SERVER_URL, REQ_SIGNUP, userid, username, email, password, 1];
    
    NSLog(@"signup request url : %@", url);
    
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSLog(@"signup response url : %@", responseObject);
        
        int result_code = [[responseObject valueForKey:RES_CODE] intValue];
        
        if (result_code == CODE_SUCCESS) {
            
            _user._idx = [[responseObject valueForKey:RES_IDX] intValue];
            
            [self uploadImage];
            
        } else if (result_code == CODE_USERID_EXIST) {
            
            [self hideLoadingView];
            
            [self showAlertDialog:nil message:EXIST_USERID positive:ALERT_OK negative:nil];
            
        } else if (result_code == CODE_WRONG_PASSWORD) {
            
            [self hideLoadingView];
            
            [self showAlertDialog:nil message:WRONG_PASSWORD positive:ALERT_OK negative:nil];
        
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
        [self hideLoadingView];
        
        [self showAlertDialog:ALERT_TITLE message:CONN_ERROR positive:ALERT_OK negative:nil];
        
    }];
    
}

- (void) uploadImage {
    
    NSString *url = [NSString stringWithFormat:@"%@%@", SERVER_URL, REQ_UPLOADIMAGE];
    
    NSDictionary * params = @{PARAM_ID : [NSNumber numberWithInt:_user._idx],
                              PARAM_IMAGETYPE : [NSNumber numberWithInt:0]
                              };
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:url parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:photoPath] name:@"file" fileName:@"filename.png" mimeType:@"image/png" error:nil];
    } error:nil];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLSessionUploadTask *uploadTask;
    
    uploadTask = [manager
          uploadTaskWithStreamedRequest:request
          progress:nil
          completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
              
              if (error) {
                  
                  NSLog(@"Error: %@", error);
                  
                  [self hideLoadingView];
                  [self showAlertDialog:nil message:UPLOAD_FAIL positive:ALERT_OK negative:nil];
              }
              
              else {
                  
                  [self hideLoadingView];
                  
                  NSLog(@"profile image respond : %@", responseObject);
                  
                  int nResultCode = [[responseObject valueForKey:RES_CODE] intValue];
                  
                  if(nResultCode == CODE_SUCCESS) {
                      
                      _user._email = tfEmail.text;
                      _user._password = tfPassword.text;
                      
                      [CommonUtils saveUserInfo];
                      
                      fromSignup = YES;
                      bAutoLogin = YES;
                      
                      [self.navigationController popViewControllerAnimated:NO];

                  } else if (nResultCode == CODE_UPLOADFAIL) {
                      [self showAlertDialog:nil message:UPLOAD_FAIL positive:ALERT_OK negative:nil];
                  }
              }
          }];
    
    [uploadTask resume];
}


- (void) gotoMain {
    
    [[UIApplication sharedApplication].keyWindow setRootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"MainTabViewController"]];    
}

#pragma mark - imagedelegate

- (IBAction)addProfile:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self camera];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [self gallery];
    }]];
    
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    //actionSheet.view.tintColor = [UIColor lightGrayColor];
    [self presentViewController:actionSheet animated:YES completion:nil];
}


- (void) camera {
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.allowsEditing = YES;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:imagePicker animated:YES completion:nil];
        
    } else {
        
        NSLog(@"No Cameran\n. Please test on device");
    }
}

- (void) gallery {
    
    UIImagePickerController * imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.allowsEditing = YES;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController: imagePicker animated:YES completion:nil];
    
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the album or taken from the camera
- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    
    UIImage * chosenImage = info[UIImagePickerControllerEditedImage];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
    [picker dismissViewControllerAnimated:YES completion:^{
        
        dispatch_queue_t writeQueue = dispatch_queue_create("SavePhoto", NULL);
        
        dispatch_async(writeQueue, ^{
            
            NSString * strPhotoPath = [CommonUtils saveToFile:chosenImage isProfile:YES];
            
            dispatch_async(dispatch_get_main_queue(), ^ {
                
                photoPath = strPhotoPath;
                
                // update ui (set profile image with saved Photo URL
                [imvProfile setImage:[UIImage imageWithContentsOfFile:strPhotoPath]];
                           });
        });
    }];
    
    // do some progress to scale with specified size and then save to local path
    // then set it to user profile photoPath
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (CGFloat) getOffsetYWhenShowKeybarod {
    
    if ([tfPassword isFirstResponder] || [tfConfirmPwd isFirstResponder]) {
        
        return 130;
    }
    
    return 220;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
