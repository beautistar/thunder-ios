//
//  HubViewController.m
//  Thunder
//
//  Created by Developer on 11/18/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "HubViewController.h"
#import "ProfileViewController.h"
#import "HubDetailViewController.h"

#import "HubListCell.h"

#import "UserEntity.h"
#import "HubEntity.h"
#import "Const.h"

//#import <CCBottomRefreshControl/UIScrollView+BottomRefreshControl.h>

@interface HubViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout> {
    
    __weak IBOutlet UITableView *tblHubList;
    
    UIRefreshControl *upperRefreshControl;
    UIRefreshControl *bottomRefreshControl;
    
    UserEntity *_user;
    
    int pageIndex;
}

@end

@implementation HubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initVars];
    
    [self initView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) initVars {
    
    pageIndex = 1;
    
    upperRefreshControl = [[UIRefreshControl alloc] init];
    bottomRefreshControl = [[UIRefreshControl alloc] init];
}

- (void) initView {
    
    tblHubList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // add pull to refresh on UITableView
    UIColor *refreshTintColor = [UIColor colorWithRed:51/255.f green:102/255.f blue:173/255.f alpha:1.0];
    [upperRefreshControl setTintColor:refreshTintColor];
    [upperRefreshControl addTarget:self action:@selector(refreshHubList:) forControlEvents:UIControlEventValueChanged];
    [tblHubList addSubview:upperRefreshControl];
    
    // add bottom refresh control on UITableView
//    bottomRefreshControl.triggerVerticalOffset = 90;
//    [bottomRefreshControl setTintColor:refreshTintColor];
//    [bottomRefreshControl addTarget:self action:@selector(refreshHubList:) forControlEvents:UIControlEventValueChanged];
    
//    tblHubList.bottomRefreshControl = bottomRefreshControl;
    tblHubList.estimatedRowHeight = 100;
    tblHubList.rowHeight = UITableViewAutomaticDimension;
    
    
//    if selectedUser != nil {
//        self.title = selectedUser!._name + " " + Constants.TITLE_TIMELINELIST_SUFFIX
//    } else {
//        self.title = _user!._name + " " + Constants.TITLE_TIMELINELIST_SUFFIX
//    }
}

- (void) refreshHubList:(UIRefreshControl *)sender {
    
    
}

//- (void) gotoFullTextView : (HubEntity *) hub {
//    
//    FullTextViewController *fullTVC = (FullTextViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"FullTextViewController"];
//    fullTVC.hub = hub;
//    [self.navigationController pushViewController:fullTVC animated:YES];
//    
//}

//- (void) gotoProfileVC : (UserEntity *) friend {
//    
//    ProfileViewController *profileVC = (ProfileViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//    profileVC._user = friend;
//    profileVC._from = FROM_FRIEND;
//    
//    [self.navigationController pushViewController:profileVC animated:YES];
//    
//}

- (void) gotoHunDetailVC : (HubEntity *) hub {
    
    HubDetailViewController *hubDetailVC = (HubDetailViewController *) [self.storyboard instantiateViewControllerWithIdentifier:@"HubDetailViewController"];
    hubDetailVC.selectedHub = hub;
    [self.navigationController pushViewController:hubDetailVC animated:YES];
    
}

- (IBAction)prepareFOrUnwindToHubList:(UIStoryboardSegue *)segue {
    
    if ([segue.identifier isEqualToString:@"unwind2HudList"]) {
        
//        HubDetailViewController * sourceVC = segue.sourceViewController;
        
        //delete hub and reload data
//        HubEntity *deletedHub = sourceVC.selectedHub;
//        for timeLine in arrTimeLine {
//            if timeLine.id == deletedTimeLine.id {
//                arrTimeLine.removeObject(timeLine)
//                break
//            }
//        }
//
//        tblTimeLineList.reloadData()
//    }
        
        
    }
}

#pragma mark - tableview delegate & datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    HubListCell *cell = (HubListCell *) [tableView dequeueReusableCellWithIdentifier:@"HubListCell"];
    
    /*
    let cell = tableView.dequeueReusableCellWithIdentifier("MainTimeLineCell") as! MainTimeLineCell
    
    cell.setContent(arrTimeLine[indexPath.row], expandAction: { _ in
        tableView.reloadData()
        if indexPath.row == self.arrTimeLine.count - 1 {
            tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: false)
        }
    }, textTapAction: { _ in
        
        let timeLine = self.arrTimeLine[indexPath.row]
        
        // if timeline content length is over 100 then it will be direct to full text
        // otherwise it will be direct to detail page
        if timeLine.content.length >= 100 {
            
            self.gotoFullTextView(timeLine)
        } else {
            
            guard timeLine.id != 0 else { return }
            self.gotoTimeLineDetail(timeLine)
        }
        
    }, photoTapAction: { _ in
        // for showing wonbridge timeline image
    }, block: { (sender) in
        
        if self.selectedUser != nil {
            
            self.navigationController?.popViewControllerAnimated(true)
            
        } else {
            
            let timeLine = self.arrTimeLine[indexPath.row]
            guard timeLine.user_id != self._user!._idx else {
                return
            }
            
            guard let selectedUser = self._user!.getFriend(timeLine.user_id) else  {
                
                // timeLine user is not a friend, so will make a temp user with timeLine then will go to user profile page
                let timeLineUser = FriendEntity()
                timeLineUser._idx = timeLine.user_id
                timeLineUser._name = timeLine.user_name
                timeLineUser._photoUrl = timeLine.photo_url
                
                self.gotoUserProfile(timeLineUser)
                return
            }
            
            self.gotoUserProfile(selectedUser)
        }        
    })
*/
    
    return cell;
    
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     var viewControllers = navigationController?.viewControllers
     
     guard viewControllers != nil && viewControllers?.count >= 2 else {
     return
     }
     
     let lastVC = viewControllers![viewControllers!.count - 2]
     
     if lastVC.isKindOfClass(TimeLineDetailViewController) {
     
     selectedTimeLine = arrTimeLine[indexPath.row]
     performSegueWithIdentifier("unwind2TimeLineDetail", sender: self)
     } else {
     // navigation stack: My Page -> TimeLineList
     let detailVC = self.storyboard?.instantiateViewControllerWithIdentifier("TimeLineDetailViewController") as! TimeLineDetailViewController
     detailVC.selectedTimeLine = arrTimeLine[indexPath.row]
     self.navigationController?.pushViewController(detailVC, animated: true)
     }
     */
    
    [self gotoHunDetailVC:nil];
    
    
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
    if cell.isKindOfClass(MainTimeLineCell) {
        let timeLineCell = cell as! MainTimeLineCell
        timeLineCell.setCollectionViewDataSourceDelegate(self, forRow: indexPath.row)
    }
    */
}

#pragma mark - collectionview delegate & datasource

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
//        return arrTimeLine[collectionView.tag].file_url.count
    return 9;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    /*
     
     let cell = collectionView.dequeueReusableCellWithReuseIdentifier("TimeLinePhotoCollectionViewCell", forIndexPath: indexPath) as! TimeLinePhotoCollectionViewCell
     let timeLine = arrTimeLine[collectionView.tag]
     cell.setContent(timeLine.file_url[indexPath.row])
     
     */   
    
    
    HubPhotoCollectionViewCell * cell = (HubPhotoCollectionViewCell *) [collectionView dequeueReusableCellWithReuseIdentifier:@"HubPhotoCollectionViewCell" forIndexPath:indexPath];
    
    return cell;
    
}

//Use for size
- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    return kTimeLineCollectionSize;
    return CGSizeMake(tblHubList.frame.size.width/3, tblHubList.frame.size.width/3);
}

@end


