//
//  HubDetailViewController.m
//  Thunder
//
//  Created by Developer on 11/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "HubDetailViewController.h"
#import <YYText/YYText.h>
#import "HubInputBar.h"

@interface HubDetailViewController () {
    
    
    __weak IBOutlet UIImageView *imvBgUserProfile;
    __weak IBOutlet UIImageView *imvProfilePhoto;
    __weak IBOutlet UILabel *lblLastLoggedTime;
    __weak IBOutlet UILabel *lblFriendStatus;
    __weak IBOutlet UILabel *lblPostedTime;
    __weak IBOutlet UILabel *lblAddress;
    
    __weak IBOutlet YYLabel *lblTimeLineText;
    
    __weak IBOutlet NSLayoutConstraint *inputbarBottomConstraint;
    __weak IBOutlet NSLayoutConstraint *inputbarHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *timeLinePhotosHeightConstraint;
    
    __weak IBOutlet UILabel *likeUsersCountLabel;
    __weak IBOutlet UILabel *replyMessageCountLabel;
    __weak IBOutlet UILabel *likeUsersCountArrowLabel;
    __weak IBOutlet UIView *likeUsersTableRootView;
    __weak IBOutlet UIButton *likeActionButton;
    
    
    __weak IBOutlet UIView *timeLinePhotosRootView;
    
    __weak IBOutlet NSLayoutConstraint *likeUserTableRootViewHeight;
    __weak IBOutlet NSLayoutConstraint *replyTableViewHeightConstraint;
    __weak IBOutlet NSLayoutConstraint *timeLineTextLabelHeightConstraint;
    
    __weak IBOutlet UITableView *replyUserTableView;
    
    __weak IBOutlet UIScrollView *superScrolView;
    __weak IBOutlet HubInputBar *inputBar;
    
    
    
    //@IBOutlet weak var inputbar: TimeLineInputbar!

    // height for timeline photos
    // will be set zero if no files in selected timeline

 


    
    // timeline photo horizontal scroll tableview
    /*private lazy var timeLinePhotosTableView: UITableView = {
        
        let tableView = UITableView(frame: CGRectMake(2, 2, kTimeLinePhotoCellHeight, UIScreen.mainScreen().bounds.width - 4))
        tableView.backgroundColor = UIColor.clearColor()
        tableView.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI / 2.0))
        tableView.center =  CGPointMake(self.view.frame.size.width / 2, kTimeLinePhotoViewHeight / 2.0 )
        
        tableView.pagingEnabled = true
        tableView.showsVerticalScrollIndicator = false
        tableView.bounces = false
        
        tableView.separatorStyle = .None
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = kTimeLinePhotoCellHeight
        
        return tableView
    }()*/
    
    
    
    /*
     
     // 有空出来聚餐啊，好久不见了~~
     //    @IBOutlet weak var lblTimeLineText: YYLabel! { didSet {
     //
     //        }}
     
     @IBOutlet weak var lblTimeLineText: YYLabel! { didSet {
     lblTimeLineText.highlightTapAction = ({ [weak self] containerView, text, range, rect in
     self!.didTapRichLabelText(self!.lblTimeLineText, textRange: range)
     })
     }
     }
     
     @IBOutlet weak var inputbar: TimeLineInputbar!

     @IBOutlet weak var timeLinePhotosRootView: UIView! { didSet {
     timeLinePhotosRootView.clipsToBounds = true
     }}
     
     // timeline photo horizontal scroll tableview
     private lazy var timeLinePhotosTableView: UITableView = {
     
     let tableView = UITableView(frame: CGRectMake(2, 2, kTimeLinePhotoCellHeight, UIScreen.mainScreen().bounds.width - 4))
     tableView.backgroundColor = UIColor.clearColor()
     tableView.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI / 2.0))
     tableView.center =  CGPointMake(self.view.frame.size.width / 2, kTimeLinePhotoViewHeight / 2.0 )
     
     tableView.pagingEnabled = true
     tableView.showsVerticalScrollIndicator = false
     tableView.bounces = false
     
     tableView.separatorStyle = .None
     tableView.dataSource = self
     tableView.delegate = self
     tableView.rowHeight = kTimeLinePhotoCellHeight
     
     return tableView
     }()
     
     // height for timeline photos
     // will be set zero if no files in selected timeline
     @IBOutlet weak var timeLinePhotosHeightConstraint: NSLayoutConstraint!
     @IBOutlet weak var likeUsersCountLabel: UILabel!
     @IBOutlet weak var replyMessageCountLabel: UILabel!
     @IBOutlet weak var likeActionButton: UIButton!
     @IBOutlet weak var likeUsersCountArrowLabel: UILabel!
     @IBOutlet weak var likeUsersTableRootView: UIView!
     
     private lazy var likeUserTableView: UITableView = {
     
     let tableView = UITableView(frame: CGRectMake(0, kLikeUserTableViewMarginTop, kTableViewWidth, UIScreen.mainScreen().bounds.width - 120))
     
     tableView.backgroundColor = UIColor.clearColor()
     tableView.transform = CGAffineTransformMakeRotation(CGFloat(-M_PI / 2.0))
     tableView.center = CGPointMake((self.view.frame.size.width - 110) / 2, 30 )
     
     tableView.pagingEnabled = true
     tableView.showsVerticalScrollIndicator = false
     tableView.bounces = false
     
     tableView.separatorStyle = .None
     tableView.dataSource = self
     tableView.rowHeight = kTableViewWidth + 2*kLikeUserCellLeftRightPadding
     
     return tableView
     } ()
     
     @IBOutlet weak var likeUserTableRootViewHeight: NSLayoutConstraint!
     @IBOutlet weak var replyUserTableView: UITableView!
     @IBOutlet weak var replyTableViewHeightConstraint: NSLayoutConstraint!
     
     @IBOutlet weak var timeLineTextLabelHeightConstraint: NSLayoutConstraint!
     
     */
}

@end

@implementation HubDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// like or dislike action

- (IBAction)actionButtonTapped:(id)sender {
    

//
//    
//    if selectedTimeLine!.user_id == _user!._idx {
//        // this will do a action for deleting my timeline.
//        showDeleteConfirmDialog()
//        
//    } else {
//        
//        // like or dislike timeline posted by other users
//        WebService.likeTimeLine(selectedTimeLine!.id, userId: _user!._idx, like: !_isLike) { (status) in
//            
//            if (status) {
//                
//                self._isLike = !self._isLike
//                
//                let me = FriendEntity()
//                me._idx = self._user!._idx
//                me._name = self._user!._name
//                me._photoUrl = self._user!._photoUrl
//                
//                if self._isLike && !self._likeUsers.contains(me) {
//                    
//                    self._likeUsers.append(me)
//                    
//                } else if !self._isLike {
//                    
//                    for index in 0 ..< self._likeUsers.count {
//                        
//                        if self._likeUsers[index]._idx == me._idx {
//                            
//                            self._likeUsers.removeAtIndex(index)
//                            break
//                        }
//                    }
//                }
//                
//                self.updateLikeUserView()
//            }
//        }
//    }
}

- (IBAction)backButtonTapped:(id)sender {
    
//    if selectedTimeLine!.user_id == _user!._idx {
    
    [self.navigationController popViewControllerAnimated:YES];
//    } else {
//        self.performSegueWithIdentifier("unwind2TimeLine", sender: self)
//    }
}

/*
 
 
 
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
