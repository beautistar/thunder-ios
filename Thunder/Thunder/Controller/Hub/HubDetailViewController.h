//
//  HubDetailViewController.h
//  Thunder
//
//  Created by Developer on 11/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HubEntity.h"

@interface HubDetailViewController : UIViewController

@property (nonatomic, strong) HubEntity *selectedHub;
@end
