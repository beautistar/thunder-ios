//
//  UserEntity.h
//  Thunder
//
//  Created by Developer on 11/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "RoomEntity.h"
#import "XmppPacket.h"
#import "FriendEntity.h"

@interface UserEntity : NSObject

@property (nonatomic) int _idx;
@property (nonatomic, strong) NSString * _name;
@property (nonatomic, strong) NSString * _email;
@property (nonatomic, strong) NSString * _password;
@property (nonatomic, strong) NSString * _label;
@property (nonatomic, strong) NSString * _bgUrl;
@property (nonatomic, strong) NSString * _photoUrl;
@property (nonatomic, strong) NSString * _phoneNumber;
@property (nonatomic) int _allowFriend;
@property (nonatomic, strong) NSString * _userName;

/////////added by me
@property (nonatomic) int _friendCount;
@property (nonatomic) int _readNotCount;


@property (nonatomic, strong) NSString * _nickName;
@property (nonatomic, strong) NSString * _nickPhoto;
@property (nonatomic, strong) NSString * _anonymousName;

@property (nonatomic) int _nickState;
@property (nonatomic) int _anonymousState;
@property (nonatomic) int _isPublicTimeline;
@property (nonatomic) int _selectedState;
@property (nonatomic) int _recommandCounter;

@property (nonatomic, strong) NSMutableArray * _friendList;
@property (nonatomic, strong) NSMutableArray * _roomList;
@property (nonatomic, strong) NSMutableArray * _postImage;

- (BOOL) isValid;

- (FriendEntity *) getFriend : (int) idx;

- (BOOL) isFriend : (int) idx;

- (RoomEntity *) getRoom:(NSString *) roomName;

- (BOOL) isBlockFriend : (int) idx;

// add or delete friend
- (void) addFriend:(UserEntity *) _other;
- (void) deleteFriend:(UserEntity *) _other;

- (BOOL) isEqual:(UserEntity *)object;

// check if a user is friend with param _user
- (BOOL) isExistFriend:(UserEntity *)_other;

// update room information with recieved packet
- (BOOL) updatedExistRoom:(XmppPacket *) _revPacket;

// update user's chat room list with received packet
- (void) updateUserRoomList:(XmppPacket *) _revPacket;

// add new room into user's chat room list
- (void) addRoomList:(RoomEntity *) _newRoom;

// return true if user is a blocked user, otherwise return false
- (BOOL) isBlockedFriend: (int) _senderIdx;



@end
