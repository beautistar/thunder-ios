//
//  UserEntity.m
//  Thunder
//
//  Created by Developer on 11/20/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "UserEntity.h"
#import "CommonUtils.h"

@implementation UserEntity

@synthesize _idx, _name, _bgUrl, _email, _label, _nickName, _password, _friendCount, _readNotCount;
@synthesize _photoUrl, _roomList, _userName, _nickPhoto, _nickState, _postImage, _friendList;
@synthesize _allowFriend, _phoneNumber, _anonymousName, _selectedState, _anonymousState, _isPublicTimeline, _recommandCounter;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _idx = 0;
        _name = @"";
        _email = @"";
        _label = @"";
        _password = @"";
        _photoUrl = @"";
        _bgUrl = @"";
        _phoneNumber = @"";
        _allowFriend = 1;
        _userName = @"";
        
        /////
        _friendCount = 0;
        _readNotCount = 0;
        
        _nickName = @"";
        _nickPhoto = @"";
        _nickState = 0;
        _anonymousName = @"";
        _anonymousState = 0;
        _isPublicTimeline = 0;
        _selectedState  = 0;
        _recommandCounter = 0;
        
        _friendList = [[NSMutableArray alloc] init];
        _roomList = [[NSMutableArray alloc] init];
        _postImage = [[NSMutableArray alloc] init];
        
    }
    
    return self;
}

- (BOOL) isValid {
    
    if(_idx > 0)
        return YES;
    
    return NO;
}

- (FriendEntity *) getFriend : (int) idx {
    
    
    for (FriendEntity * friend in _friendList) {
        
        if (friend._idx == _idx) {
            
            return friend;
        }
    }
    
    return nil;
}

- (BOOL) isFriend : (int) idx {
    
    for (FriendEntity *friend in _friendList) {
        
        if (friend._idx == idx) {
            
            return YES;
        }
    }
    
    return NO;
}

- (RoomEntity *) getRoom : (NSString *) roomName {
    
    for (RoomEntity *room in _roomList) {
        
        if ([room._name isEqualToString:roomName]) {
            
            return room;
        }
    }
    
    return nil;
}

- (BOOL) isBlockFriend : (int) idx {
    
    for (FriendEntity *entity in _friendList) {
        
        if (entity._idx == _idx && entity._blockStatus == 0)
            
            return YES;
    }
    
    return NO;
    
}

- (void) addFriend:(UserEntity *)_other {
    
    for(UserEntity * _existFriend in _friendList) {
        if(_existFriend._idx == _other._idx) {
            return;
        }
    }
    
    _friendCount ++;
    [_friendList addObject:_other];
}

- (void) deleteFriend:(UserEntity *) _other {
    
    for(UserEntity * _existFriend in _friendList) {
        if(_existFriend._idx == _other._idx) {
            _friendCount --;
            [_friendList removeObject:_existFriend];
            return;
        }
    }
}

- (BOOL) isEqual:(UserEntity *)object {
    
    if(_idx == object._idx) {
        return YES;
    }
    
    return NO;
}

- (BOOL) isExistFriend:(UserEntity *)_other {
    
    for(UserEntity * _existFriend in _friendList) {
        if(_existFriend._idx == _other._idx) {
            return YES;
        }
    }
    
    return NO;
}

//- (UserEntity *) getFriend:(int) idx {
//    
//    for(UserEntity * friend in _friendList) {
//        
//        if (friend._idx == idx) {
//            
//            return friend;
//        }
//    }
//    
//    return nil;
//}

//- (BOOL) isBlockedUser: (int) _senderIdx {
//
//    for (int i = 0; i < _blockList.count; i ++) {
//
//        if (_senderIdx == [_blockList[i] intValue]) {
//
//            return YES;
//        }
//    }
//
//    return NO;
//}

- (BOOL) updatedExistRoom:(XmppPacket *) _revPacket {
    
    for(RoomEntity * _entity in _roomList) {
        
        // upadte chat room information with none read message
        // in case of inviting -  update participants, participants name, room displayname
        if([_revPacket._roomName isEqualToString:_entity._name]) {
            
            if(_revPacket._chatType == _TEXT) {
                _entity._recentContent = _revPacket._bodyString;
            } else {
                _entity._recentContent = NSLocalizedString(@"Sent_File", nil);
            }
            
            _entity._recentTime = _revPacket._sentTime;
            _entity._recentCounter ++;
            
            // in case that room name is same but the participants are changed - inviting
            if(![_entity._groupName isEqualToString:_revPacket._participantName]) {
                
                _entity._groupName = _revPacket._participantName;
                [_entity updateParticipantWithPartName:_revPacket._participantName withBlock:nil];
            }
            
            // update db
            [[DBManager getSharedInstance] updateRoomWithName:_entity._name participant_name:_entity._participantName recentMessage:_entity._recentContent recentTime:_entity._recentTime recetCounter:_entity._recentCounter];
            
            //notReadCount ++;
            //            [APPDELEGATE notifyReceiveNewMessage];
            
            return YES;
        }
    }
    
    return NO;
}

// update chat room information with received message
// if user has not current room, then add it into users's room list
- (void) updateUserRoomList:(XmppPacket *) _revPacket {
    
    [CommonUtils vibrate];
    
    // check user'r room list, then if user has the current room, update it with received message
    if(![self updatedExistRoom:_revPacket]) {
        
        // get other user's idx for get his information
        NSArray *ids = [_revPacket._participantName componentsSeparatedByString:@"_"];
        
        int other = 0;
        for(int j = 0; j < ids.count; j ++) {
            
            int idx = [ids[j] intValue];
            
            if (idx != _idx) {
                
                other = idx;
                break;
            }
        }
        
        // get user's info and then add new chat room into user's room ist
        NSMutableArray * _arrParticipants = [NSMutableArray array];
        NSString * url = [NSString stringWithFormat:@"%@%@/%d", SERVER_URL, REQ_GETFRIENDLIST, other];
        
        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
            
            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
            
            if(nResult_Code == CODE_SUCCESS) {
                
                NSDictionary *_dictParticipant = [responseObject objectForKey:RES_USERINFO];
                
                UserEntity *_entity = [[UserEntity alloc] init];
                _entity._idx = [[_dictParticipant valueForKey:RES_ID] intValue];
                
                //                _entity._firstName = [_dictParticipant valueForKey:RES_FIRSTNAME];
                //                _entity._lastName = [_dictParticipant valueForKey:RES_LASTNAME];
                //
                //                _entity._major = [[_dictParticipant valueForKey:RES_MAJOR] intValue];
                //                _entity._year = [[_dictParticipant valueForKey:RES_YEAR] intValue];
                //                _entity._sex = [[_dictParticipant valueForKey:RES_SEX] intValue];
                //                _entity._status = [[_dictParticipant valueForKey:RES_STATUS] intValue];
                //                _entity._interest = [[_dictParticipant valueForKey:RES_INTEREST] intValue];
                //
                //                _entity._favMovie = [_dictParticipant valueForKey:RES_MOVIE];
                //                _entity._aboutMe = [_dictParticipant valueForKey:RES_ABOUTME];
                //                _entity._photoUrl = [_dictParticipant valueForKey:RES_PHOTOURL];
                //
                //                _entity._friendCount = [[_dictParticipant valueForKey:RES_FRIENDCOUNT] intValue];
                
                [_arrParticipants addObject:_entity];
                
                // create new chat room
                ////////////////////////////
                //RoomEntity * _newRoom = [[RoomEntity alloc] initWithName:_revPacket._roomName participants:_arrParticipants];
                
                // update with new info
                /////////////////////
                //_newRoom._recentContent = _revPacket._bodyString;
                //////////////////////////////
                //_newRoom._recentDate = _revPacket._sentTime;
                //_newRoom._recentCounter = 1;
                
                //[self addRoomList:_newRoom];
            }
            
        } failure:^(NSURLSessionTask *operation, NSError *error) {
            
            NSLog(@"Error: %@", error);
        }];
    }
}

- (void) addRoomList:(RoomEntity *) _newRoom {
    
    // increase none read message count as user has received new message
    // before adding a room, check user's room list again, because of ansynchronize commnunication
    _readNotCount ++;
    //    [APPDELEGATE notifyReceiveNewMessage];
    
    for(RoomEntity * _existRoom in _roomList) {
        
        // already exist room
        if([_existRoom._name isEqualToString:_newRoom._name]) {
            _existRoom._recentContent = _newRoom._recentContent;
            _existRoom._recentTime = _newRoom._recentTime;
            _existRoom._recentCounter ++;
            
            // get room display name with participants.
            if(![_existRoom._participantName isEqualToString:_newRoom._participantName]) {
                [_existRoom makeRoomDisplayName];
            }
            
            // add a room into local database
            // it is done by with only one functions - adding & updating
            [[DBManager getSharedInstance] saveRoomWithName:_newRoom._name participant_name:_newRoom._participantName recent_message:_newRoom._recentContent recent_time:_newRoom._recentTime recent_counter:_newRoom._recentCounter];
            
            return;
        }
    }
    
    [_roomList addObject:_newRoom];
    
    // add a room into local database
    [[DBManager getSharedInstance] saveRoomWithName:_newRoom._name participant_name:_newRoom._participantName recent_message:_newRoom._recentContent recent_time:_newRoom._recentTime recent_counter:_newRoom._recentCounter];
}

// if blocked friend, return YES...

- (BOOL) isBlockedFriend:(int)_friendIdx {
    
    FriendEntity * _friend = [self getFriend:_friendIdx];
    if(_friend == nil) {
        return NO;
    }
    else {
        return (_friend._blockStatus == 0);
    }
}


@end
