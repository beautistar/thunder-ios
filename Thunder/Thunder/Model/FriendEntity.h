//
//  FriendEntity.h
//  Thunder
//
//  Created by Developer on 12/6/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FriendEntity : NSObject

@property (nonatomic) int _idx;
@property (nonatomic, strong) NSString *_name;
@property (nonatomic, strong) NSString *_phoneNumber;
@property (nonatomic, strong) NSString *_label;
@property (nonatomic, strong) NSString *_bgUrl;
@property (nonatomic, strong) NSString *_photoUrl;
@property (nonatomic) int _blockStatus;
@property (nonatomic) BOOL _isSelected;
@property (nonatomic) BOOL _isFriend;
@property (nonatomic) int _requestState;

- (BOOL) equal : (NSObject *) obj;
@end
