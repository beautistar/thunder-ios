//
//  RoomEntity.m
//  Thunder
//
//  Created by Developer on 12/13/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "RoomEntity.h"
#import "UserDefault.h"
#import "ReqConst.h"
#import "DBManager.h"

@implementation RoomEntity

@synthesize _name, _participantName, _participantList, _participantListState, _recentContent, _recentTime;
@synthesize _recentCounter, _ownerIdx, _groupName, _groupPhoto, _ownerState, _isSelected;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _name = @"";
        _participantName = @"";
        _recentContent = @"";
        _recentTime = @"";
        _groupName = @"";
        _groupPhoto = @"";
        
        _ownerState = 0;
        _ownerIdx = 0;
        _recentCounter  = 0;
        _isSelected = NO;
        
        _participantList = [[NSMutableArray alloc] init];
        _participantListState = [[NSMutableArray alloc] init];
    }
    
    return self;
}

// make a room as selection friend

//// when make initial room - making room by selecting a friend

- (instancetype) initWithParticipants:(NSMutableArray *) participants {
    
    if(self = [self init]) {
        
        _participantList = participants;
        _name = [self makeRoomName];
        _participantName = [self makeParticipantName];
    }
    
    return self;
}

//- (instancetype) initWithName:(NSString *) name participants:(NSString *) participants recentContent:(NSString *) recentContent recentTime:(NSString*) recentTime recentCounter:(int)recentCounter owner:(int)owner {
//    
//    if (self = [self init]) {
//        
//        _name = name;
//        _recentContent = recentContent;
//        _participantName = participants;
//        _recentTime = recentTime;
//        _recentCounter = recentCounter;
//        _ownerIdx = owner;
//    }
//    
//    return self;
//}

- (void) makeRoolRoom {
    
    int roostate = 100000000 + _ownerState;
    
    _name = [NSString stringWithFormat:@"%@%s%d%s%d", _name, "_", roostate, "_", _ownerIdx];
}

- (NSString *) makeRoomName {
    
    NSString * roomName = @"";
    NSMutableArray * ids = [[NSMutableArray alloc] init];
    
    for (FriendEntity * entity in _participantList) {
        
        [ids addObject:[NSNumber numberWithInt:entity._idx]];
    }
    
    [ids addObject:[NSNumber numberWithInt:APPDELEGATE.Me._idx]];
    
    NSArray * _sortedArray=  [ids sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    for(int i = 0; i < [_sortedArray count]; i ++) {
        roomName = [NSString stringWithFormat:@"%@_%i", roomName, [(NSNumber *)_sortedArray[i] intValue]];
    }
    
    roomName = [roomName substringFromIndex:1];
    
    return roomName;
}

- (NSString *) makeParticipantName {
    
    NSString  * _paticipantsName = @"";
    
    NSMutableArray * _nameArray = [NSMutableArray array];
    
    for (FriendEntity * _friend in _participantList) {
        [_nameArray addObject:[NSNumber numberWithInt:_friend._idx]];
    }
    
    [_nameArray addObject:[NSNumber numberWithInt:APPDELEGATE.Me._idx]];
    
    NSArray * _sortedArray=  [_nameArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    for(int i = 0; i < [_sortedArray count]; i ++) {
        _paticipantsName = [NSString stringWithFormat:@"%@_%i", _paticipantsName, [(NSNumber *)_sortedArray[i] intValue]];
    }
    
    _paticipantsName = [_paticipantsName substringFromIndex:1];
    
    return _paticipantsName;
}

- (NSString *) getDisplayName {
    
    NSString *displayName = @"";
    NSMutableArray *ids = [[NSMutableArray alloc] init];
    
    for (FriendEntity *friend in _participantList) {
        
        [ids addObject:[NSNumber numberWithInt:friend._idx]];
    }
    
    NSArray * _sortedIds =  [ids sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        return [obj1 compare:obj2];
    }];
    
    
    for (int i = 0 ; i < [_sortedIds count] ; i++ ) {
        
        for (FriendEntity *entity in _participantList) {
            
            if ((int)_sortedIds[i] == entity._idx) {
                
                displayName = [NSString stringWithFormat:@"%@%@%s", displayName, entity._name, ", "];
                break;
            }
        }
    }
    
    if (displayName.length > 2) {
        
        NSRange needleRange = NSMakeRange(0, displayName.length - 2);
        displayName = [displayName substringWithRange:needleRange];
    }
    
    return displayName;
}

- (NSString *) getDisplayCount {
    
    NSString *displayCount = @"";
    
    if ([_participantList count] >= 2) {
        
        displayCount = [NSString stringWithFormat:@"%@%s%d%s", displayCount, " (", (int)_participantList.count + 1, ")"];
    }
    
    return displayCount;
    
}

// idx - chatting friend idx
// check if there is a friend with idx in participants...


- (FriendEntity *) getParticipant:(int) idx {
    
    for(FriendEntity * friend in _participantList) {
        
        if (friend._idx == idx) {
            
            return friend;
        }
    }
    
    return nil;
    
}

- (BOOL) isEqual:(id) other {
    
    RoomEntity *_other = (RoomEntity *) other ;
    return ([_other._name isEqualToString:_name]);
    
}

- (BOOL) isGroup {
    
    if (_participantList.count > 1 ) {
        
        return YES;
    }
    return NO;
}

//////////




- (instancetype) initWithName:(NSString *) roomName participants:(NSMutableArray *)participants {
    
    
    if(self = [self init]) {
        
        _name = roomName;
        _participantList = participants;
        _participantName = [self makeParticipantName];
        
//        _displayName = [self makeRoomDisplayName];
    }
    
    return self;
}

//- (NSString *) makeRoomName {
//    
//    NSString * roomName = @"";
//    
//    NSMutableArray * _nameArray = [NSMutableArray array];
//    
//    for(UserEntity * _user in _participants) {
//        [_nameArray addObject:[NSNumber numberWithInt:_user._idx]];
//    }
//    
//    [_nameArray addObject:[NSNumber numberWithInt:APPDELEGATE.Me._idx]];
//    
//    NSArray * _sortedArray=  [_nameArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
//        return [obj1 compare:obj2];
//    }];
//    
//    for(int i = 0; i < [_sortedArray count]; i ++) {
//        roomName = [NSString stringWithFormat:@"%@_%i", roomName, [(NSNumber *)_sortedArray[i] intValue]];
//    }
//    
//    roomName = [roomName substringFromIndex:1];
//    
//    return roomName;
//}

// participant name include user's id
// participant array doesn't include users id.
//- (NSString * ) makeParticipantsName {
//    
//    NSString  * _partsName = @"";
//    
//    NSMutableArray * _nameArray = [NSMutableArray array];
//    
//    for(UserEntity * _user in _participants) {
//        [_nameArray addObject:[NSNumber numberWithInt:_user._idx]];
//    }
//    
//    [_nameArray addObject:[NSNumber numberWithInt:APPDELEGATE.Me._idx]];
//    
//    NSArray * _sortedArray=  [_nameArray sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
//        return [obj1 compare:obj2];
//    }];
//    
//    for(int i = 0; i < [_sortedArray count]; i ++) {
//        _partsName = [NSString stringWithFormat:@"%@_%i", _partsName, [(NSNumber *)_sortedArray[i] intValue]];
//    }
//    
//    _partsName = [_partsName substringFromIndex:1];
//    
//    return _partsName;
//}

- (NSString *) makeRoomDisplayName {
    
    NSString * roomDisplayName = @"";
    
    NSArray *_sortedArray = [_participantList sortedArrayUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        NSNumber * first = [NSNumber numberWithInt:((UserEntity*)obj1)._idx];
        NSNumber * second = [NSNumber numberWithInt:((UserEntity*)obj2)._idx];
        
        return [first compare:second];
    }];
    
    for(UserEntity * _user in _sortedArray) {
        roomDisplayName = [NSString stringWithFormat:@"%@, %@", roomDisplayName, _user._name];
    }
    
    roomDisplayName = [roomDisplayName substringFromIndex:1];
    
    return roomDisplayName;
}

- (BOOL) equals:(id) other {
    
    RoomEntity *_other = (RoomEntity *) other;
    return ([_other._name isEqualToString:_name]);
}

/**
 **     in case of received invite message, when to invite other user at the room
 **     participantsName : 1_2_3...... update with received name...
 **     change self._participantsName......with new name
 **/
- (void) updateParticipantWithPartName:(NSString *) participantsName withBlock:(void (^)(void)) updatedRoomDisplayName{
    
    
    //        UserEntity * _user = APPDELEGATE.Me;
    //
    //        // make server url
    //        NSString * url = [NSString stringWithFormat:@"%@%@/%@", SERVER_URL, REQ_GET_ROOM_INFO, participantsName];
    //
    //        AFHTTPSessionManager * manager = [AFHTTPSessionManager  manager];
    //        [manager GET:url parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
    //
    //            int nResult_Code = [[responseObject valueForKey:RES_CODE] intValue];
    //
    //            if(nResult_Code == CODE_SUCCESS) {
    //
    //                NSMutableArray * _roomParticipants = [responseObject objectForKey:RES_USER_LIST];
    //
    //                for (int i = 0; i < [_roomParticipants count]; i ++) {
    //
    //                    NSDictionary * _dict = _roomParticipants[i];
    //
    //                    int _roomParticipantIdx = [[_dict valueForKey:RES_IDD] intValue];
    //
    //                    if(_roomParticipantIdx == APPDELEGATE.Me._idx) continue;
    //
    //                    FriendEntity * roomParticipant = [self getParticipant:_roomParticipantIdx];
    //
    //                    if(roomParticipant == nil) {
    //
    //                        if([_user getFriend:_roomParticipantIdx] != nil) {
    //
    //                            [_participants addObject:[_user getFriend:_roomParticipantIdx]];
    //
    //                        } else {
    //
    //                            FriendEntity * roomParticipant = [[FriendEntity alloc] init];
    //                            roomParticipant._idx = [[_dict valueForKey:RES_IDD] intValue];
    //                            roomParticipant._name = [_dict valueForKey:RES_NAME];
    //                            roomParticipant._email = [_dict valueForKey:RES_EMAIL];
    //                            roomParticipant._phoneNumber = [_dict valueForKey:RES_PHONE_NUMBER];
    //                            roomParticipant._photoUrl = [_dict valueForKey:RES_PHOTO_URL];
    //                            roomParticipant._bgUrl = [_dict valueForKey:RES_BG_URL];
    //                            roomParticipant._label = [_dict valueForKey:RES_LABEL];
    //
    //                            [_participants addObject:roomParticipant];
    //                        }
    //                    }
    //                }
    //
    //                _displayName = [self makeRoomDisplayName];
    //
    //                if(updatedRoomDisplayName)
    //                    updatedRoomDisplayName();
    //            }
    //
    //        } failure:^(NSURLSessionTask *operation, NSError *error) {
    //
    //            NSLog(@"Error: %@", error);
    //
    //            [self updateParticipantWithPartName:participantsName withBlock:updatedRoomDisplayName];
    //        }];
}

/**
 **     inviting other user
 **     update participant with param... participants
 **     ... to invite friends...
 **/

- (void) updateParticiapntWithParticipants: (NSMutableArray *) newParticipants {
    
    [_participantList addObjectsFromArray:newParticipants];
    
    // update _participant name and _displayName
//    _name = [self makeParticipantName];
    _participantName = [self makeParticipantName];
    
    [[DBManager getSharedInstance] updateRoomWithName:_name participant_name:_name];
    _name = [self makeRoomDisplayName];
}

- (int) getCurrentUsers {
    return (int)[_participantList count];
}


@end
