//
//  XmppPacket.m
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import "XmppPacket.h"
#import "Const.h"

@implementation XmppPacket

@synthesize _chatType, _bodyString, _sentTime, _from, _roomName, _participantName, _fileName;
@synthesize _isDelayed, _isNew;
@synthesize _timestamp, _date;
@synthesize _bShowProgress, _progressValue;
@synthesize height;

- (instancetype) init {
    
    if(self = [super init]) {
        // initialize code here
        _chatType = _TEXT;
        _bodyString = @"";
        _fileName = @"";
        _sentTime = @"";
        _from = @"";
        _date = @"";
        _isDelayed = NO;
        _isNew = NO;
        _bShowProgress = NO;
        _progressValue = 0;
        height = 42.f;      // UITableViewCell estimate app
        
        
    }
    
    return self;
}

- (instancetype) initWithType:(NSString *)roomName participantName:(NSString *) participantName senderName:(NSString *) senderName chatType:(ChatType) chatType body:(NSString *) bodyString fileName:(NSString *)fileName sendTime:(NSString *) sendTime {
    
    if(self = [super init]) {
        
        // initialize code here
        _roomName = roomName;                   // chat room name
        _participantName = participantName;     // chat room participant name
        _chatType = chatType;                   // TEXT, IMAGE, VIDEO, FILE
        _bodyString = bodyString;               // message string
        _fileName = fileName;                   // in case of file transferring,... it include real filename in local...
        _timestamp = sendTime;                   // sending time
        _from = senderName;                     // sener's name
    }
    
    return self;
}

// packet to message body string to send
// room#roomJID#message#20151212 오전(오후), 12:00:00
// room#roomJID#IMAGE#message#fileName#20151212 오전(오후), 12:00:00
- (NSString *) toStringWithProtocol {
    
    NSString * _message = @"";
    
    if(_chatType == _IMAGE) {
        _message = KEY_IMAGE_MARKER;
    } else if(_chatType == _FILE) {
        _message = KEY_FILE_MARKER;
    } else if(_chatType == _VIDEO) {
        _message = KEY_VIDEO_MARKER;
    }
    
    _message = [NSString stringWithFormat:@"%@%@", _message, _bodyString];
    
    if(_chatType != _TEXT)
        _message = [NSString stringWithFormat:@"%@#%@", _message, _fileName];
    
    // add room marker and room name into message
    _message = [NSString stringWithFormat:@"%@%@:%@:%@#%@", KEY_ROOM_MARKER, _roomName, _participantName, _from, _message];
    
    _message = [NSString stringWithFormat:@"%@#%@", _message, _timestamp];
    
    //    NSLog(@"sending message: %@", _message);
    
    return _message;
}

// get packet from body string
- (XmppPacket *) fromStringWithProtocol:(NSString *) message from:(NSString *) sender {
    
    _roomName= [self getRoomName:message];
    _participantName = [self getParticipantName:message];
    _chatType = [self getType:message];
    _bodyString = [self getMessage:message];
    _fileName = [self getFileName:message];
    _timestamp = [self getTime:message];
    _date = [self getDate:_timestamp];
    _sentTime = [self getDisplayTime:_timestamp];
    _from = sender;
    
    return self;
}

//  get roomJID from message body string
// room#roomJID#message#20151212 오전(오후), 12:00:00
// room#roomJID#IMAGE#message#fileName#20151212 오전(오후), 12:00:00
- (NSString *) getRoomName:(NSString *) body {
    
    NSString * roomName = @"";
    roomName = (NSString *)((NSArray *)[body componentsSeparatedByString:@"#"][1]);
    roomName = [roomName substringToIndex:[roomName rangeOfString:@":"].location];
    return roomName;
}

- (NSString *) getParticipantName:(NSString *) body {
    
    NSString * participantName = @"";
    participantName = (NSString *)((NSArray *)[body componentsSeparatedByString:@"#"][1]);
    participantName = (NSString *)((NSArray *)[participantName componentsSeparatedByString:@":"][1]);
    return participantName;
}

- (int) getParticipantsCount {
    
    NSArray * _participants = [_participantName componentsSeparatedByString:@"_"];
    
    return (int)(_participants.count - 1);
}

//  get message body from message string with protocol
//  room#roomJID#IMAGE#message#20151212 오전(오후), 12:00:00
//  room#roomJID#message#20151212 오전(오후), 12:00:00          ----------text
- (NSString *) getMessage:(NSString *) body {
    
    NSString * message = @"";
    if(_chatType == _TEXT) {
        message = (NSString *)((NSArray*)[body componentsSeparatedByString:@"#"][2]);
    } else {
        message = (NSString *)((NSArray*)[body componentsSeparatedByString:@"#"][3]);
    }
    return message;
}

// room#roomJID#message#20151212 오전(오후), 12:00:00
// room#roomJID#IMAGE#message#fileName#20151212 오전(오후), 12:00:00
- (NSString *) getFileName:(NSString *) body {
    
    NSString * filename = @"";
    if(_chatType != _TEXT)
    {
        filename = (NSString *)((NSArray*)[body componentsSeparatedByString:@"#"][4]);
    }
    return filename;
}

- (NSString *) getTime:(NSString *) body {
    
    NSString * time = [body substringFromIndex:[body rangeOfString:KEY_TIME_SEPERTATOR options:NSBackwardsSearch].location + 1];
    return time;
}

- (NSString *) getDate:(NSString *) time {
    
    NSArray * date_time = [time componentsSeparatedByString:@","];
    
    NSString * date = date_time[0];
    return date;
}

- (NSString *) getDisplayTime:(NSString *) time {
    
    NSArray * date_time = [time componentsSeparatedByString:@","];
    
    NSString * dispTime = date_time[1];
    
    dispTime = [dispTime substringToIndex:[dispTime rangeOfString:@":" options:NSBackwardsSearch].location];
    
    NSArray * arrHourMin = [dispTime componentsSeparatedByString:@":"];
    
    int _hour = [arrHourMin[0] intValue];
    NSString * _minute = arrHourMin[1];
    
    if(_hour < 12) {
        dispTime = [NSString stringWithFormat:@"%@ %i:%@", NSLocalizedString(@"TIME_AM", nil), _hour, _minute];
    } else {
        
        _hour -= 12; if(_hour == 0) _hour = 12;
        dispTime = [NSString stringWithFormat:@"%@ %i:%@", NSLocalizedString(@"TIME_PM", nil), _hour, _minute];
    }
    
    return dispTime;
}

- (ChatType) getType:(NSString *) body {
    
    ChatType _type = _TEXT;
    
    if([body containsString:KEY_IMAGE_MARKER]) {
        _type = _IMAGE;
    } else if([body containsString:KEY_FILE_MARKER]) {
        _type = _FILE;
    } else if([body containsString:KEY_VIDEO_MARKER]) {
        _type = _VIDEO;
    }
    
    return _type;
}

@end
