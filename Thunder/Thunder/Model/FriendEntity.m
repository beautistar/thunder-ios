//
//  FriendEntity.m
//  Thunder
//
//  Created by Developer on 12/6/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import "FriendEntity.h"

@implementation FriendEntity

@synthesize _idx, _name, _phoneNumber, _label, _photoUrl, _bgUrl, _blockStatus, _isFriend, _isSelected, _requestState;

- (instancetype) init {
    
    if (self = [super init]) {
        
        // initalize code here
        
        _idx = 0;
        _name = @"";
        _label = @"";
        _photoUrl = @"";
        _bgUrl = @"";
        _phoneNumber = @"";
        _blockStatus = 1;
        _isSelected = NO;
        _requestState = 0;
        _isFriend = NO;
        
    }
    
    return self;
}

- (BOOL) equal : (NSObject *) obj {
    
    FriendEntity *other = (FriendEntity *) obj;
    
    if (_idx == other._idx)
        
        return YES;
    
    return NO;
}



@end
