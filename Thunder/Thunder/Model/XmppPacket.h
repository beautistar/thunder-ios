//
//  XmppPacket.h
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

// chatting type
typedef enum {
    _TEXT = 0, _IMAGE, _VIDEO, _FILE
} ChatType;


///////////////////
//   room#roomJID#IMAGE#message#20151212 오전(오후), 12:00:00
////////////////////


@interface XmppPacket : NSObject {
    
    ChatType _chatType;                 // 문자열, 이미지, 비데오, 파일전송..
    NSString * _bodyString;             // 텍스트메시지 혹을 서버에 업로드한 파일의 url
    NSString * _fileName;                // 업로드파일네임 : (이미지, 비데오, 파일전송시 전송한 파일네임)
    NSString * _sentTime;               // 보낸 시간
    NSString * _from;                   // 보낸 사람
    NSString * _roomName;               // 채팅 room name
    NSString * _participantName;        // 채팅 참가자수
    
    NSString * _timestamp;             // timestamp of incoming message
    NSString * _date;
    
    BOOL _isDelayed;                    // 과거 메시지인가?
    
    BOOL _isNew;                        // new receive message
    
    BOOL _bShowProgress;       //
    int _progressValue;          //
    
    
    CGFloat height;
}


@property (nonatomic) ChatType _chatType;
@property (nonatomic, strong) NSString * _bodyString;
@property (nonatomic, strong) NSString * _sentTime;
@property (nonatomic, strong) NSString * _roomName;
@property (nonatomic, strong) NSString * _participantName;
@property (nonatomic, strong) NSString * _from;
@property (nonatomic, strong) NSString * _fileName;
@property (nonatomic, strong) NSString * _timestamp;
@property (nonatomic, strong) NSString * _date;
@property (nonatomic) BOOL _isDelayed;
@property (nonatomic) BOOL _isNew;
@property (nonatomic) BOOL _bShowProgress;
@property (nonatomic) int _progressValue;
@property (nonatomic) CGFloat height;

- (instancetype) initWithType:(NSString *)roomName participantName:(NSString *) participantName senderName:(NSString *) senderName chatType:(ChatType) chatType body:(NSString *) bodyString fileName:(NSString *)fileName sendTime:(NSString *) sendTime;

- (NSString *) toStringWithProtocol;
- (XmppPacket *) fromStringWithProtocol:(NSString *) message from:(NSString *) sender;
- (int) getParticipantsCount;

@end
