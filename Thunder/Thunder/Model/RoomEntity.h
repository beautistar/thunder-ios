//
//  RoomEntity.h
//  Thunder
//
//  Created by Developer on 12/13/16.
//  Copyright © 2016 Developer. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FriendEntity.h"



@interface RoomEntity : NSObject

@property (nonatomic, strong) NSString * _name;
@property (nonatomic, strong) NSString * _participantName;
@property (nonatomic, strong) NSString * _recentContent;
@property (nonatomic, strong) NSString * _recentTime;
@property (nonatomic, strong) NSString * _groupName;
@property (nonatomic, strong) NSString * _groupPhoto;
@property (nonatomic) int _recentCounter;
@property (nonatomic) int _ownerIdx;
@property (nonatomic) int _ownerState;
@property (nonatomic) BOOL _isSelected;

@property (nonatomic, strong) NSMutableArray * _participantList;
@property (nonatomic) NSMutableArray * _participantListState;


- (void) makeRoolRoom;

- (NSString *) makeParticipantName;

- (NSString *) getDisplayName;

- (NSString *) getDisplayCount;

- (FriendEntity *) getParticipant:(int) idx;

- (BOOL) isEqual:(id)object;

- (BOOL) isGroup;

- (instancetype) initWithParticipants:(NSMutableArray *) participants;
- (instancetype) initWithName:(NSString *) roomName participants:(NSMutableArray *)participants;
//- (instancetype) initWithName:(NSString *) name participants:(NSString *) participants recentContent:(NSString *) recentContent recentTime:(NSString*) recentTime recentCounter:(int)recentCounter owner:(int)owner;

- (void) updateParticipantWithPartName:(NSString *) participantsName withBlock:(void(^)(void)) updatedRoomDisplayName;
- (void) updateParticiapntWithParticipants: (NSMutableArray *) newParticipants;

- (NSString *) makeRoomDisplayName;

- (int) getCurrentUsers;

@end
