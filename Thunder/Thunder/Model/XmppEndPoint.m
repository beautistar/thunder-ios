//
//  XmppEndPoint.m
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "XmppEndPoint.h"
#import "UserEntity.h"
#import "ReqConst.h"
#import "Const.h"
#import "UserDefault.h"

// Log levels: off, error, warn, info, verbose
#if DEBUG
static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
static const int ddLogLevel = LOG_LEVEL_INFO;
#endif

@interface XmppEndPoint ()

@property (nonatomic, strong) UserEntity * user;

- (void)setupStream;
- (void)teardownStream;

- (void)goOnline;
- (void)goOffline;

@end

@implementation XmppEndPoint

//@synthesize isXmppConnected;
//@synthesize xmppStream;
//@synthesize xmppReconnect;
//@synthesize xmppRoomJIDPaused;
//
//@synthesize xmppJoinRoom, xmppJoinRoomStorage;
//@synthesize _messageDelegate, _roomMessageDelegate, _reconnectionDelegte, _friendRequestDelegate;
//
//
//@synthesize isSentFriendRequest;

@synthesize isXmppConnected;
@synthesize xmppStream;
@synthesize xmppReconnect;
@synthesize xmppRoomJIDPaused;

@synthesize xmppJoinRoom, xmppJoinRoomStorage;
@synthesize _messageDelegate, _roomMessageDelegate, _reconnectionDelegte;

- (instancetype) init
{
    
    if(self = [super init])
    {
        // initialize code here
        
        // Configure logging framework
        [DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:XMPP_LOG_FLAG_SEND_RECV];
    }
    
    return self;
}

- (instancetype) initWithHostName:(NSString *) p_strHostName hostPort:(int) p_nHostPort
{
    self = [super init];
    
    if(self)
    {
        // initialize code here
        
        // Configure logging framework
        
        [DDLog addLogger:[DDTTYLogger sharedInstance] withLogLevel:XMPP_LOG_FLAG_SEND_RECV];
        
        hostName = p_strHostName;
        hostPort = p_nHostPort;
        conferenceService = [NSString stringWithFormat:@"%@.%@", @"conference", hostName];
        
        xmppStream = nil;
        xmppReconnect = nil;
        
        xmppJoinRoom = nil;
        xmppJoinRoomStorage = nil;
        
        userId = -1;
        password = nil;
        
        _user = APPDELEGATE.Me;
    }
    
    return self;
}

- (void) setupStream
{
    //
    // Setup xmpp stream
    //
    // The XMPPStream is the base class for all activity.
    // Everything else plugs into the xmppStream, such as modules/extensions and delegates.
    //
    xmppStream = [[XMPPStream alloc] init];
    
    // Setup reconnect
    //
    // The XMPPReconnect module monitors for "accidental disconnections" and
    // automatically reconnects the stream for you.
    // There's a bunch more information in the XMPPReconnect header file.
    
    xmppReconnect = [[XMPPReconnect alloc] init];
    
    //
    // Activate xmpp modules
    //
    [xmppReconnect          activate:xmppStream];
    
    //
    // Add ourself as a delegate to anything we may be interested in
    //
    [xmppStream addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    
    // Optional:
    //
    // Replace me with the proper domain and port.
    // The example below is setup for a typical google talk account.
    //// Optional:
    //
    // Replace me with the proper domain and port.
    // The example below is setup for a typical google talk account.
    //
    // If you don't supply a hostName, then it will be automatically resolved using the JID (below).
    // For example, if you supply a JID like 'user@quack.com/rsrc'
    // then the xmpp framework will follow the xmpp specification, and do a SRV lookup for quack.com.
    //
    // If you don't specify a hostPort, then the default (5222) will be used.
    
    //	[xmppStream setHostName:@"talk.google.com"];
    //	[xmppStream setHostPort:5222];
    
    //
    // Setup server information
    //
    xmppStream.hostName = hostName;
    xmppStream.hostPort = hostPort;
    
    // You may need to alter these settings depending on the server you're connecting to
    customCertEvaluation = YES;
}

- (void) teardownStream
{
    [xmppStream removeDelegate:self];
    
    [xmppReconnect deactivate];
    [xmppStream disconnect];
    
    xmppStream = nil;
    xmppReconnect = nil;
}

- (void) goOnline
{
    
    if(xmppStream != nil) {
        XMPPPresence *presence = [XMPPPresence presence]; // type="available" is implicit
        [xmppStream sendElement:presence];
    }
    
    if(xmppRoomJIDPaused != nil) {
        [self EnterRoomInBg:xmppRoomJIDPaused];
    }
    
    //
    // openfireAPNS 서비스에서 푸시메시지토큰 해제
    //
    
    [self registerDeviceToken:@""];
}

- (void) goOffline
{
    
    if(xmppStream != nil) {
        
        XMPPPresence *presence = [XMPPPresence presenceWithType:@"unavailable"];
        [xmppStream sendElement:presence];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark Connect/disconnect
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL) connect:(int) p_nUserId password:(NSString *)p_strPwd
{
    if(xmppStream != nil) {
        if(![xmppStream isDisconnected]) {
            return YES;
        }
    }
    
    // myJID = @"user@gmail.com/D-Chat";
    // myPassword = @"******";
    
    NSString *myJID = [NSString stringWithFormat:@"%i@%@/%@", p_nUserId, hostName, XMPP_RESOURCE];
    
    [xmppStream setMyJID:[XMPPJID jidWithString:myJID]];
    
    password = p_strPwd;
    
    NSError *error = nil;
    if(![xmppStream connectWithTimeout:XMPPStreamTimeoutNone error:&error]) {
        
        DDLogError(@"Error connecting: %@", error);
        
        return NO;
    }
    
    return YES;
}

- (void) disconnect
{
    
    [self goOffline];
    
    if(xmppStream !=  nil) {
        
        [xmppStream disconnect];
    }
}

////////////////////////////////////////////////////////////////////////////////////
// MARK: - OpenfireAPNS 서비스연동.
////////////////////////////////////////////////////////////////////////////////////

//    <iq type="set" to="OPENFIRE_SERVER" id="apns68057d6a">
//    <query xmlns="urn:xmpp:apns">
//    <token>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</token>
//    </query>
//    </iq>

- (void) registerDeviceToken:(NSString *) deviceToken {
    
    DDXMLElement * query = [DDXMLElement elementWithName:@"query" xmlns:@"urn:xmpp:apns"];
    DDXMLElement * token = [DDXMLElement elementWithName:@"token" stringValue:deviceToken];
    [query addChild:token];
    
    XMPPIQ * iq = [XMPPIQ iqWithType:@"set" to:[XMPPJID jidWithString:hostName] elementID:@"apns68057d6a" child:query];
    
    if(xmppStream != nil)
        [xmppStream sendElement:iq];
}

////////////////////////////////////////////////////////////////////////////////////
#pragma mark - XMPPStream delegate
////////////////////////////////////////////////////////////////////////////////////

- (void)xmppStream:(XMPPStream *)sender socketDidConnect:(GCDAsyncSocket *)socket
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStreamDidConnect:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    isXmppConnected = YES;
    
    NSError *error = nil;
    
    if (![xmppStream authenticateWithPassword:password error:&error])
    {
        DDLogError(@"Error authenticating: %@", error);
    }
}

- (void)xmppStreamDidDisconnect:(XMPPStream *)sender withError:(NSError *)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    if(xmppStream != nil) {
        if(xmppJoinRoom != nil) {
            xmppRoomJIDPaused = xmppJoinRoom.myRoomJID;
            
        } else {
            xmppRoomJIDPaused = nil;
        }
    }
    
    if(bIsShownChatView)
        [_reconnectionDelegte xmppDisconnected];
    
    if (!isXmppConnected)
    {
        DDLogError(@"Unable to connect to server. Check xmppStream.hostName");
    }
}
-(void)xmppStreamDidAuthenticate:(XMPPStream *)sender
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    [self goOnline];
    
}

- (void)xmppStream:(XMPPStream *)sender didNotAuthenticate:(NSXMLElement *)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

/**
 * XMPP 메시지 수신 처리부.
 */
- (void)xmppStream:(XMPPStream *)sender didReceiveMessage:(XMPPMessage *)message
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    if([message isChatMessageWithBody]) {
        
        NSString * msg = [[message elementForName:@"body"] stringValue];
        NSString *_from = [[message attributeForName:@"from"] stringValue];
        
        NSArray * _arrFrom = [_from componentsSeparatedByString:@"@"];
        NSString * _sender = (NSString *)_arrFrom[0];
        
        if([_user isBlockedFriend:[_sender intValue]])
            return;
        
        XmppPacket * _revPacket = [[XmppPacket alloc] init];
        _revPacket = [_revPacket fromStringWithProtocol:msg from:_sender];
        
        NSString * _roomName = [xmppJoinRoom.myRoomJID user];
        
        // 방의 이름이 현재 유저가 들어가 있는 방의 이름이면....
        if([_revPacket._roomName isEqualToString:_roomName]) {
            
            if([[message elementForName:@"delay"] stringValue] != nil) {
                [_roomMessageDelegate newRoomPackedReceived:_revPacket];
            }
            
            [_messageDelegate newPacketReceived:_revPacket];
            return;
        }
        
        // save chat with _revPacket
        [[DBManager getSharedInstance] saveChatWithRoomName:_revPacket._roomName content:[_revPacket toStringWithProtocol]  from:[_revPacket._from intValue] date:_revPacket._timestamp current:0];
        
        //        [[DBManager getSharedInstance] saveChatWithRoomName:_revPacket._roomName content:[_revPacket toStringWithProtocol]  from:[_revPacket._from intValue] date:_revPacket._timestamp];
        
        if(bIsShownChatList) {
            [_messageDelegate newPacketReceived:_revPacket];
        } else {
            [_user updateUserRoomList:_revPacket];
        }
    }
}

- (BOOL)xmppStream:(XMPPStream *)sender didReceiveIQ:(XMPPIQ *)iq
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    return NO;
}

- (void)xmppStream:(XMPPStream *)sender didReceiveError:(id)error
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
}

- (void)xmppStream:(XMPPStream *)sender didReceivePresence:(XMPPPresence *)presence
{
    DDLogVerbose(@"%@: %@ - %@", THIS_FILE, THIS_METHOD, [presence fromStr]);
}

////////////////////////////////////////////////////////////////////////////////////
// MARK: - XMPPRoom delegate
////////////////////////////////////////////////////////////////////////////////////

- (void) xmppRoomDidCreate:(XMPPRoom *)sender {
    
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    [sender configureRoomUsingOptions:nil];
}

- (void) xmppRoomDidJoin:(XMPPRoom *)sender
{
    DDLogVerbose(@"xmppRoomDidJoin");
    
    if(xmppRoomJIDPaused != nil) {
        xmppRoomJIDPaused = nil;
    }
    
    [_reconnectionDelegte xmppConnected];
}

- (void) xmppRoomDidLeave:(XMPPRoom *)sender
{
    DDLogVerbose(@"xmppRoomDidLeave");
}

// 친구가 방으로 들어온 경우 .....
- (void) xmppRoom:(XMPPRoom *)sender occupantDidJoin:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence {
    
}

// 친구가 방에서 나간 경우 .....
- (void) xmppRoom:(XMPPRoom *)sender occupantDidLeave:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence
{
    
}

// .... 친구가 방으로 들어오거나 나간 경우......
- (void) xmppRoom:(XMPPRoom *)sender occupantDidUpdate:(XMPPJID *)occupantJID withPresence:(XMPPPresence *)presence
{
    
}

// 방안에서 메시지수신
// from="1_2@conference.52.68.134.121/1
- (void) xmppRoom:(XMPPRoom *)sender didReceiveMessage:(XMPPMessage *)message fromOccupant:(XMPPJID *)occupantJID
{
    DDLogVerbose(@"%@: %@", THIS_FILE, THIS_METHOD);
    
    // 그룹채팅, 메시지내용이 있는가 검사
    if([message isMessageWithBody]) {
        
        NSString *msg = [[message elementForName:@"body"] stringValue];
        NSString *_from = [[message attributeForName:@"from"] stringValue];
        
        NSArray * _arrFrom = [_from componentsSeparatedByString:@"/"];
        
        if(_arrFrom.count < 2) return;
        
        NSString * _sender = (NSString *)_arrFrom[1];
        
        //        if([_user isBlockedFriend:[_sender intValue]] || ([_sender intValue] == _user._idx)) {
        //            return;
        //        }
        // 자기자신이 보낸 메시지는 처리하지 않는다
        if([_sender intValue] == _user._idx)
            return;
        
        
        XmppPacket * _revPacket = [[XmppPacket alloc] init];
        _revPacket = [_revPacket fromStringWithProtocol:msg from:_sender];
        
        _revPacket._isNew = YES;
        
        [_roomMessageDelegate newRoomPackedReceived:_revPacket];
    }
}

#pragma mark -
#pragma mark - create and join room with room information - RoomEntity

//////////////
//  채팅방으로 방을 창조하면서 들어간다. (유저가 채팅창에서 방을 선택할때)
/////////////

- (void) EnterChattingRoom :(RoomEntity *) _room {
    
    
    NSString * virtualDomain = [xmppStream.myJID domain];
    
    // roomname@conference.121.88.250.136
    
    NSString * jid = [NSString stringWithFormat:@"%@@%@.%@", _room._name, @"conference", virtualDomain];
    
    XMPPJID * _roomJID = [XMPPJID jidWithString:jid];
    
    xmppJoinRoomStorage = [[XMPPRoomMemoryStorage alloc] init];
    
    xmppJoinRoom = [[XMPPRoom alloc] initWithRoomStorage:xmppJoinRoomStorage jid:_roomJID dispatchQueue:dispatch_get_main_queue()];
    
    [xmppJoinRoom activate:xmppStream];
    [xmppJoinRoom addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
    [history addAttributeWithName:@"maxstanzas" stringValue:@"0"];
    
    [xmppJoinRoom joinRoomUsingNickname:xmppStream.myJID.user history:history password:nil];
}

////////////////
//   대화방에서 탈퇴
////////////////
-(void) leaveRoomWithJID:(RoomEntity *)_room {
    
    if (!xmppJoinRoom) return;
    
    [xmppJoinRoom leaveRoom];
    [xmppJoinRoom removeDelegate:self];
    [xmppJoinRoom deactivate];
    
    xmppJoinRoomStorage = nil;
    xmppJoinRoom = nil;
}

//////////////
// 앱이 배경으로 들어갈때 xmpp방에서 탈퇴
- (void) leaveRoomInBg {
    
    if(!xmppJoinRoom) return;
    
    [xmppJoinRoom leaveRoom];
    [xmppJoinRoom removeDelegate:self];
    [xmppJoinRoom deactivate];
    
    xmppJoinRoom = nil;
    xmppJoinRoomStorage = nil;
}

////////////
// 앱이 배경에서 나왔을때 xmpp방으로 재입장
- (void) EnterRoomInBg:(XMPPJID *) roomJID {
    
    xmppJoinRoomStorage = [[XMPPRoomMemoryStorage alloc] init];
    
    xmppJoinRoom = [[XMPPRoom alloc] initWithRoomStorage:xmppJoinRoomStorage jid:roomJID dispatchQueue:dispatch_get_main_queue()];
    
    [xmppJoinRoom activate:xmppStream];
    [xmppJoinRoom addDelegate:self delegateQueue:dispatch_get_main_queue()];
    
    NSXMLElement *history = [NSXMLElement elementWithName:@"history"];
    [history addAttributeWithName:@"maxstanzas" stringValue:@"0"];
    
    [xmppJoinRoom joinRoomUsingNickname:xmppStream.myJID.user history:history password:nil];
}


// 대화친구에게 메시지보내기 (일반메시지).... 대화친구가 아직 방안에 있지 않는 경우
- (void) sendPacket:(XmppPacket *)_sendPacket to:(int)friendIdx {
    
    //    XMPPMessage *  message = [XMPPMessage messageWithType:@"chat" to:[XMPPJID jidWithString:[NSString stringWithFormat:@"%i@%@/%@", friendIdx, XMPP_SERVER_URL, XMPP_RESOURCE]]];
    //    [message addBody:[_sendPacket toStringWithProtocol]];
    //    [xmppStream sendElement:message];
    
    NSXMLElement * body = [NSXMLElement elementWithName:@"body"];
    [body setStringValue:[_sendPacket toStringWithProtocol]];
    NSXMLElement * message = [NSXMLElement elementWithName:@"message" xmlns:@"jabber:client"];
    
    [message addAttributeWithName:@"from" stringValue:[xmppStream.myJID bare]];
    [message addAttributeWithName:@"to" stringValue:[[XMPPJID jidWithString:[NSString stringWithFormat:@"%i@%@/%@", friendIdx, XMPP_SERVER_URL, XMPP_RESOURCE]] full]];
    [message addAttributeWithName:@"type" stringValue:@"chat"];
    [message addChild:body];
    
    NSLog(@"send message =  %@", message);
    [xmppStream sendElement:message];
}

//  대화친구들에게 메시지 보내기 (방메시지)
- (void) sendPacket:(XmppPacket *)_sendPacket  {
    
    [xmppJoinRoom sendMessageWithBody:[_sendPacket toStringWithProtocol]];
}

@end
