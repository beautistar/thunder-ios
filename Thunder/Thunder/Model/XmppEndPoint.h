//
//  XmppEndPoint.h
//  SFinder
//
//  Created by AOC on 27/11/16.
//  Copyright © 2016 Mobile. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMPPFramework.h"
#import "DCMessageDelegate.h"

#import "FriendEntity.h"
#import "RoomEntity.h"
#import "XmppPacket.h"

@class UserEntity;

@interface XmppEndPoint : NSObject <XMPPStreamDelegate, XMPPRoomDelegate> {
    
//    NSString * hostName;
//    int hostPort;
//    NSString * conferenceService;
//    
//    XMPPStream * xmppStream;
//    XMPPReconnect * xmppReconnect;
//    
//    XMPPRoom * xmppJoinRoom;                        // chatting room when user chat with his or friend, entering room
//    XMPPRoomMemoryStorage * xmppJoinRoomStorage;
//    
//    XMPPJID * xmppRoomJIDPaused;                    // room id when the app is entering in a bakcground     
//    int userId;                                     // xmpp user id & password
//    NSString * password;
//    
//    BOOL isXmppConnected;
//    BOOL customCertEvaluation;
//    
//    __unsafe_unretained NSObject <DCMessageDelegate> * _messageDelegate;                    // message receive delegate outside of room
//    __unsafe_unretained NSObject <DCRoomMessageDelegate> * _roomMesssageDelegate;           // message receive delegate inside of room
//    __unsafe_unretained NSObject <XmppCustomReconnectionDelegate> *_reconnectionDelegate;   // xmpp reconnection delegate
//    __unsafe_unretained NSObject <XmppFriendRequestDelegate> *_friendRequestDelegate;
//    
//    BOOL isSentFriendRequest;
    
    NSString * hostName;
    int hostPort;
    NSString * conferenceService;
    
    XMPPStream * xmppStream;
    XMPPReconnect * xmppReconnect;
    
    XMPPRoom * xmppJoinRoom;                        // 채팅시 입장할 대화방
    XMPPRoomMemoryStorage * xmppJoinRoomStorage;
    
    XMPPJID * xmppRoomJIDPaused;                    // 앱이 배경으로 들어갈때 당시의 방JID
    
    int userId;
    NSString * password;
    
    BOOL isXmppConnected;
    
    BOOL customCertEvaluation;
    
    __unsafe_unretained NSObject <DCMessageDelegate> * _messageDelegate;            // 방바깥 메시지수신 델리케이트
    __unsafe_unretained NSObject <DCRoomMessageDelegate> * _roomMesssageDelegate;    // 방안 메시지...
    
    __unsafe_unretained NSObject <XmppCustomReconnectionDelegate> *_reconnectionDelegate;  // xmpp reconnection delegate
}

//@property (nonatomic) BOOL isXmppConnected;
//@property (nonatomic, strong, readonly) XMPPStream *xmppStream;
//@property (nonatomic, strong, readonly) XMPPReconnect *xmppReconnect;
//
//@property (nonatomic, strong, readonly) XMPPRoom * xmppJoinRoom;
//@property (nonatomic, strong, readonly) XMPPRoomMemoryStorage * xmppJoinRoomStorage;
//
//@property (nonatomic, retain) XMPPJID * xmppRoomJIDPaused;
//
//@property (nonatomic, assign) id _messageDelegate;
//@property (nonatomic, assign) id _roomMessageDelegate;
//@property (nonatomic, assign) id _reconnectionDelegte;
//@property (nonatomic, assign) id _friendRequestDelegate;
//
//@property (nonatomic) BOOL isSentFriendRequest;
//
//- (instancetype) initWithHostName:(NSString *) p_strHostName hostPort:(int) p_nHostPort;
//
////- (BOOL) connect;
//- (void) setupStream;
//- (void) teardownStream;
//- (BOOL) connect:(int) p_nUserId password:(NSString *)p_strPwd;
//- (void) disconnect;
//
//- (void) sendPacket:(XmppPacket *)_sendPacket;
//- (void) sendPacket:(XmppPacket *)_sendPacket to:(int)friendIdx;
//
//- (void) EnterChattingRoom :(RoomEntity *) _room;
//- (void) enterChattingRoomForFriendRequest: (RoomEntity *) requestRoom;
//-(void) leaveRoomWithJID:(RoomEntity *)_room;
//
//- (void) EnterRoomInBg:(XMPPJID *) roomJID;
//- (void) leaveRoomInBg;
//
//- (void) registerDeviceToken:(NSString *) deviceToken;

//// from d-chat //-----
@property (nonatomic) BOOL isXmppConnected;
@property (nonatomic, strong, readonly) XMPPStream *xmppStream;
@property (nonatomic, strong, readonly) XMPPReconnect *xmppReconnect;

@property (nonatomic, strong, readonly) XMPPRoom * xmppJoinRoom;
@property (nonatomic, strong, readonly) XMPPRoomMemoryStorage * xmppJoinRoomStorage;

@property (nonatomic, retain) XMPPJID * xmppRoomJIDPaused;

@property (nonatomic, assign) id _messageDelegate;
@property (nonatomic, assign) id _roomMessageDelegate;
@property (nonatomic, assign) id _reconnectionDelegte;

- (instancetype) initWithHostName:(NSString *) p_strHostName hostPort:(int) p_nHostPort;

//- (BOOL) connect;
- (void) setupStream;
- (void) teardownStream;
- (BOOL) connect:(int) p_nUserId password:(NSString *)p_strPwd;
- (void) disconnect;

//- (void) sendMessage:(NSString *) messageStr to:(int)friendIdx;
//- (void) sendMessage:(NSString *) messageStr;
- (void) sendPacket:(XmppPacket *)_sendPacket;
- (void) sendPacket:(XmppPacket *)_sendPacket to:(int)friendIdx;

- (void) EnterChattingRoom :(RoomEntity *) _room;
-(void) leaveRoomWithJID:(RoomEntity *)_room;

- (void) EnterRoomInBg:(XMPPJID *) roomJID;
- (void) leaveRoomInBg;

- (void) registerDeviceToken:(NSString *) deviceToken;


@end
